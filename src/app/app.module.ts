import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material';
import { CustomMaterialModule } from './core/material.module';
import { LoginComponent } from './screens/public/login/login.component';
// Formulário
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
// Expansivo
import { MatExpansionModule } from '@angular/material/expansion';
// SnackBar
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { DashboardModule } from './screens/private/admin/dashboard/dashboard.module';
import { PanelModule } from './screens/private/doctor/panel/panel.module';
// FlexLayout
import { FlexLayoutModule } from '@angular/flex-layout';
// Máscara de campos
import { NgxMaskModule } from 'ngx-mask';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatExpansionModule,
    MatSnackBarModule,
    FlexLayoutModule,
    CustomMaterialModule,
    DashboardModule,
    PanelModule,
    NgxMaskModule.forRoot()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
