import { EventEmitter } from '@angular/core';

export class EventEmitterService {
  private static emitters: {
    [eventName: string]: EventEmitter<any>;
  } = {};

  /**
   * Realiza o tratamento e emissão do evento.
   * @param eventName - Nome do evento
   */
  static get(eventName: string): EventEmitter<any> {
    if (!this.emitters[eventName]) {
      this.emitters[eventName] = new EventEmitter<any>();
    }
    return this.emitters[eventName];
  }
}
