import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class UtilsService {
  constructor(private router: Router) {
  }

  /**
   * Exibe a rota atual.
   */
  public getCurrentUrl() {
    return this.router.url;
  }

  /**
   * Define as colunas da tabela.
   * @param dataTable - Objeto da tabela
   */
  public defineRows(dataTable: any) {
    Object.keys(dataTable).forEach(column => {
      Object.keys(dataTable[column].info).forEach(data => {
        dataTable[column].columns.push(data);
      });
    });
  }

  /**
   * Recebe o evento de click na linha da tabela.
   * @param row - Linha da tabela
   * @param route - Rota de desejada
   */
  public getRowClick(row: any, route: string) {
    const id = row.id ? row.id : row.user_id;
    this.navigateTo(`${route}/${id}`);
  }

  /**
   * Navega para a rota desejada.
   * @param route - Rota de navegação
   */
  public navigateTo(route: string) {
    this.router.navigate([route]);
  }

  /**
   * Verifica se a rota filha está de acordo com a regra estabelecida,
   * redireciona para o pai o caso contrário.
   * @param route - Rota atual
   */
  public resolveChildrenRoute(route) {
    const childrenRules = /(\/(\d)+$|\/(new))/;
    const ruleLastPart = /(\/[^\/]+$)/;
    if (route.split('/').length > 3 &&
      !childrenRules.test(route)) {
      this.navigateTo(route.replace(ruleLastPart, ''));
    }
  }

  /**
   * Resolve possíveis rotas para o usuário criar um novo registro.
   * @param route - Rota atual
   */
  public newChild(route) {
    const routeParts = route.lastIndexOf('/');
    const routeSolution = route.split('/').length === 4 ? route.substr(0, routeParts) : route;
    this.navigateTo(routeSolution + '/new');
  }
}
