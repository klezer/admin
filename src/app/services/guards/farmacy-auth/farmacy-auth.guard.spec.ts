import { TestBed, async, inject } from '@angular/core/testing';

import { FarmacyAuthGuard } from './farmacy-auth.guard';

describe('FarmacyAuthGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FarmacyAuthGuard]
    });
  });

  it('should ...', inject([FarmacyAuthGuard], (guard: FarmacyAuthGuard) => {
    expect(guard).toBeTruthy();
  }));
});
