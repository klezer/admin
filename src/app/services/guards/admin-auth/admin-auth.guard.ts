import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { RestService } from './../../rest/rest.service';

@Injectable({
  providedIn: 'root'
})
export class AdminAuthGuard implements CanActivate {
  constructor(private restService: RestService) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      return this.restService.getLoggedUser().then((profile: any) => {
        localStorage.setItem('apiVersion', 'apiVersionAdmin');
        return this.restService.canAccess() && JSON.parse(profile).is_admin;
      });
  }

}
