import { TestBed, async, inject } from '@angular/core/testing';

import { DoctorAuthGuard } from './doctor-auth.guard';

describe('DoctorAuthGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DoctorAuthGuard]
    });
  });

  it('should ...', inject([DoctorAuthGuard], (guard: DoctorAuthGuard) => {
    expect(guard).toBeTruthy();
  }));
});
