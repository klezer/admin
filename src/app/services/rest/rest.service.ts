import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from './../../../environments/environment';
import { MatSnackBar } from '@angular/material';
import { ConnectionService } from 'ng-connection-service';
import { EventEmitterService } from './../event-emitter/event-emitter.service';

export interface Notification {
  type: string;
  message: string;
}

@Injectable({
  providedIn: 'root'
})

export class RestService {
  private isConnected = true;
  private apiPath = {
    // api: `${environment.apiUrl}/api/v4`,
    api: `http://172.28.20.55/api`,
    apiVersionDoctor: 'v2',
    apiVersionAdmin: 'v4'
  };

  constructor(private connectionService: ConnectionService,
              private router: Router,
              private snackBar: MatSnackBar) {
    this.monitoringConnection();
  }

  /**
   * Verifica a versão da API de acordo com o perfil logado.
   */
  private getCurrentApiVersion() {
    return this.apiPath.api +
    `/${localStorage.getItem('apiVersion') === null ?
    this.apiPath.apiVersionAdmin :
    this.apiPath[localStorage.getItem('apiVersion')]}`;
  }

  /**
   * Realiza o monitoramento de conexão do dispositivo.
   */
  private monitoringConnection() {
    this.connectionService.monitor().subscribe(isConnected => {
      this.isConnected = isConnected;
      if (!isConnected) {
        this.snackBar.open('Você está Offline, por favor, se conecte a internet.', '', {
          duration: 0,
          panelClass: 'error'
        });
      } else {
        this.snackBar.dismiss();
      }
    });
  }

  /**
   * Restga o status da conexão dos dispositivo.
   */
  public getConnectionStatus() {
    return this.isConnected;
  }

  /**
   * Salva o Token de autorização de consumo da API no localstorage.
   * @param accessToken - Token de autenticação
   */
  public setApiServiceTokenApi(accessToken: string) {
    return localStorage.setItem('apiServiceToken', accessToken);
  }

  /**
   * Pega o Token de autorização de consumo da API.
   */
  public getApiServiceToken() {
    return localStorage.getItem('apiServiceToken');
  }

  /**
   * Salva o Token de autorização de consumo da API no localstorage.
   * @param userData - Dados do usuário autenticado
   */
  public setUserProfile(userData: string) {
    return new Promise((resolve) => {
      localStorage.setItem('userProfile', userData);
      resolve(userData);
    });
  }

  /**
   * Pega os dados do usuário logado caso contrário o encaminha para fora.
   */
  public getLoggedUser() {
    return new Promise((resolve) => {
      const logged = localStorage.getItem('userProfile');
      if (logged) {
        resolve(logged);
      } else {
        resolve(logged);
        this.router.navigate(['/login']);
      }
    });
  }

  /**
   * Verifica se o usuário está logado e com acesso.
   */
  public canAccess() {
    return localStorage.getItem('userProfile') !== null && localStorage.getItem('apiServiceToken') !== null;
  }

 /**
  * Formata de forma serializa o objeto para comunicar-se com a API.
  * @param params - Objeto para tratamento
  */
  private formatData(params: object) {
    const formated = Object.keys(params).map((key) => {
      return key + '=' + params[key];
    }).join('&');
    return formated;
  }

  /**
   * Realiza a postagem dos dados.
   * @param route - Rota da API
   * @param obj - Objeto de postagem
   * @param serialized - Se desejar alterar o formato do objeto
   * @param notifications - Tipos de notificações desejadas
   */
  public postAll(route: string, obj: any, serialized: boolean, notifications: boolean|Array<string>) {
    return new Promise((resolve) => {
      const controller = new AbortController();
      const { signal } = controller;
      fetch(`${this.getCurrentApiVersion()}/${route}`, {
        method: 'POST',
        signal,
        headers: new Headers({
          Authorization: 'Bearer ' + this.getApiServiceToken(),
          'Content-Type': serialized
            ? 'application/x-www-form-urlencoded;charset=UTF-8'
            : 'application/json',
          'X-Requested-With': 'XMLHttpRequest'
        }),
        body: serialized ? this.formatData(obj) : JSON.stringify(obj)
      })
        .then(r => { this.getStatusAuthotization(r.status); return r.json(); })
        .then(response => {
          console.dir(response);
          const finalResponse = response.data ? response.data : response;
          this.showNotificationForUser(finalResponse, notifications);
          resolve(finalResponse);
        })
        .catch(e => {
          this.showFeedback({
            type: 'error',
            message: e.message
          });
        });
      this.abortRequest(controller);
    });
  }

  /**
   * Realiza a busca de todos os registros.
   * @param route - Rota da API
   * @param obj - Parâmetro da requisição
   * @param notifications - Tipos de notificações desejadas
   */
  public getAll(route: string, obj: any, notifications: boolean|Array<string>) {
    const finalRoute = obj == null ? `${route}` : `${route}/${obj}`;
    return new Promise((resolve) => {
      const controller = new AbortController();
      const { signal } = controller;
      fetch(`${this.getCurrentApiVersion()}/${finalRoute}`, {
        method: 'GET',
        signal,
        headers: new Headers({
          Accept: 'application/json',
          Authorization: 'Bearer ' + this.getApiServiceToken(),
          'Content-Type': 'application/x-www-form-urlencoded',
          'X-Requested-With': 'XMLHttpRequest'
        })
      })
        .then(r => { this.getStatusAuthotization(r.status); return r.json(); })
        .then(response => {
          const finalResponse = response.data ? response.data : response;
          this.showNotificationForUser(finalResponse, notifications);
          resolve(finalResponse);
        })
        .catch(e => {
          this.showFeedback({
            type: 'error',
            message: e.message
          });
        });
      this.abortRequest(controller);
    });
  }

  /**
   * Realiza a busca de todos os registros paginados.
   * @param route - Rota da API
   * @param obj - Objeto de postagem
   * @param page - Página desejada
   * @param notifications - Tipos de notificações desejadas
   * @param search - Parâmetro pesquisado
   * @param perPage - Quantidade de registros desejados
   */
  public getAllPaginate(route: string,
                        obj: any,
                        page: number,
                        notifications: boolean|Array<string>,
                        search: string|boolean = false,
                        perPage = 10) {
    return new Promise((resolve, reject) => {
      const controller = new AbortController();
      const { signal } = controller;

      const partRoute = (obj) ?
      `${this.getCurrentApiVersion()}/${route}/${obj}/${perPage}` :
      `${this.getCurrentApiVersion()}/${route}/${perPage}`;
      const finalRoute = (!search) ?
      `${partRoute}?page=${page}` :
      `${partRoute}/${search}?page=${page}`;

      fetch(finalRoute, {
        method: 'GET',
        signal,
        headers: new Headers({
          Authorization: 'Bearer ' + this.getApiServiceToken(),
          Accept: 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded',
          'X-Requested-With': 'XMLHttpRequest'
        })
      })
        .then(r => { this.getStatusAuthotization(r.status); return r.json(); })
        .then(response => {
          this.showNotificationForUser(response, notifications);
          resolve(response);
        })
        .catch(e => {
          reject([]);
          this.showFeedback({
            type: 'error',
            message: e.message
          });
        });
      this.abortRequest(controller);
    });
  }

  /**
   * Realiza a busca um registro específico.
   * @param route - Rota da API
   * @param id - referência da busca
   */
  public getById(route: string, id: number) {
    return new Promise((resolve) => {
      const controller = new AbortController();
      const { signal } = controller;
      fetch(`${this.getCurrentApiVersion()}/${route}/${id}`, {
        method: 'GET',
        signal,
        headers: new Headers({
          Authorization: 'Bearer ' + this.getApiServiceToken()
        })
      })
        .then(r => { this.getStatusAuthotization(r.status); return r.json(); })
        .then((response: any) => {
          const finalResponse = response.data ? response.data : response;
          resolve(finalResponse);
        })
        .catch(e => {
          this.showFeedback({
            type: 'error',
            message: e.message
          });
        });
      this.abortRequest(controller);
    });
  }

  /**
   * Realiza a remoção de um registro específico.
   * @param route - Rota da API
   * @param id - referência da busca
   */
  public deleteById(route: string, id: number) {
    return new Promise((resolve) => {
      const controller = new AbortController();
      const { signal } = controller;
      fetch(`${this.getCurrentApiVersion()}/${route}/${id}`, {
        method: 'DELETE',
        signal,
        headers: new Headers({
          Authorization: 'Bearer ' + this.getApiServiceToken()
        })
      })
        .then(r => { this.getStatusAuthotization(r.status); return r.json(); })
        .then((response: any) => {
          const finalResponse = response.data ? response.data : response;
          resolve(finalResponse);
        })
        .catch(e => {
          this.showFeedback({
            type: 'error',
            message: e.message
          });
        });
      this.abortRequest(controller);
    });
  }

  /**
   * Realiza o download do arquivo específico.
   * @param route - Rota da API
   * @param obj - objeto de referência
   */
  public getFile(route: string, obj: object) {
    const controller = new AbortController();
    const { signal } = controller;
    const finalRoute = obj == null ? `${route}` : `${route}/${obj}`;
    return new Promise((resolve, reject) => {
      fetch(finalRoute, {
        method: 'GET',
        signal,
        headers: new Headers({
          Authorization: 'Bearer ' + this.getApiServiceToken(),
          'X-Requested-With': 'XMLHttpRequest'
        })
      }).then(res => res.blob())
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
      this.abortRequest(controller);
    });
  }

  /**
   * Realiza requisições externas.
   */
  public getAllExternal(route, obj) {
    const controller = new AbortController();
    const { signal } = controller;
    return new Promise((resolve, reject) => {
      fetch(`${route}/${obj}`, {
        method: 'GET',
        signal,
        headers: new Headers({
            Accept: 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded'
        }),
      })
      .then((r) => r.json())
        .then((response) => {
          const finalResponse = (response.data !== undefined) ? response.data : response;
          resolve(finalResponse);
        })
        .catch((e) => {
          reject(new Error(e.message));
        });
      this.abortRequest(controller);
      });
  }
  /**
   * Realiza o abortamento da requisição.
   * @param controller - Controlador da requisição
   */
  private abortRequest(controller: AbortController) {
    if (!this.getConnectionStatus()) {
      controller.abort();
      EventEmitterService.get('abortedRequest').emit(!this.isConnected);
    } else {
      EventEmitterService.get('abortedRequest').emit(!this.isConnected);
    }
  }

  /**
   * Exibe as notificações das resquisições ao usuário.
   * @param finalResponse - Objeto de resposta da requisição
   * @param notifications - Tipos de notificações desejadas
   */
  public showNotificationForUser(finalResponse: any, notifications: boolean|Array<string>) {
    const group = finalResponse.errors || finalResponse.status ? 'error' : 'success';
    if (
      Array.isArray(notifications) &&
      notifications.filter(item => item.toUpperCase() === group.toUpperCase())
        .length
    ) {
      this.showFeedback({
        type: group,
        message: finalResponse.success
          ? finalResponse.success.message
            ? finalResponse.success.message
            : finalResponse.success
          : finalResponse
      });
    }
  }

  /**
   * Exibe as mensagens retornadas na requisição.
   * @param notification - Tipo de notificação
   */
  private showFeedback(notification: any) {
    if (notification.type === 'error' || notification.type === 'errors' || notification.type === 'message') {
      notification.message = this.errorHandling(notification.message);
    }
    this.snackBar.open(notification.message, '', {
      duration: 3000,
      panelClass: notification.type
    });
  }

  /**
   * Trata os erros informados pela API.
   * @param errors - Objeto a ser tratado
   */
  private errorHandling(errors: any) {
    let result: string;
    const moreErrors = [];
    if (typeof errors === 'string') {
      result = errors;
    } else {
      Object.keys(errors).forEach(field => {
        const error = errors[field];
        Object.keys(error).forEach((key: string) => {
          if (Array.isArray(error[key])) {
            error[key].forEach(msg => {
              moreErrors.push(msg);
            });
            result = moreErrors.length > 1 ? moreErrors.join() : moreErrors[0];
          } else {
            result = error[key];
          }
        });
      });
    }
    return result;
  }

  /**
   * Desloga o usuário caso não esteja autorizado a consumir a API.
   * @param status - Status da requisição
   */
  private getStatusAuthotization(status: number) {
    if (status === 401) {
      this.doLoggout();
    }
  }

  /**
   * Apagada os dados de login do usuário
   * e o manda para a tela de login.
   */
  public doLoggout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }

}
