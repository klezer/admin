import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './screens/public/login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AdminAuthGuard } from './services/guards/admin-auth/admin-auth.guard';
import { DoctorAuthGuard } from './services/guards/doctor-auth/doctor-auth.guard';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {
    path: 'dashboard', loadChildren: './screens/private/admin/dashboard/dashboard.module#DashboardModule',
    canActivate: [AdminAuthGuard]
  },
  {
    path: 'panel', loadChildren: './screens/private/doctor/panel/panel.module#PanelModule',
    canActivate: [DoctorAuthGuard]
  },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
