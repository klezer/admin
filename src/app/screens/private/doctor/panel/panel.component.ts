import { Component, OnInit, AfterContentInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { RestService } from './../../../../services';
import { element } from 'protractor';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss']
})
export class PanelComponent implements OnInit, AfterContentInit {
  public panel = /\/(panel)$/;
  public panelChild = /(\/panel\/)|(\/\d+)|(\/new)/g;
  public childrenRules = /([a-z])*.((\/\d+)|(new))/;
  public itsOnDashboard: boolean;
  public childrenConfig = {
    panel: { title: 'Paciente', class: 'color-default', color: '#55D3AE' },
    patients: { title: 'Paciente', class: 'color-patient', color: '#2196f3' },
    doctors: { title: 'Médico', class: 'color-medical', color: '#009688' },
    administrators: { title: 'Administrador', class: 'color-administrator', color: '#212121' },
    pharmacies: { title: 'Farmácia', class: 'color-pharmacie', color: '#673ab7' },
    pharmacists: { title: 'Farmacêutico', class: 'color-pharmacist', color: '#ff5722' },
    medicines: { title: 'Remédio', class: 'color-medicine', color: '#e4131b' }
  };
  public myRoute = '/panel';
  public childRoute: string;
  public currentMenuItem = {
    label: 'Panel'
  };
  public childMenuItem = {
    label: 'Item',
    class: 'color-default'
  };
  public menuItems = [
    {
      label: 'Pacientes',
      link: '/panel/patients',
      icon: 'person_outline'
    },
    {
      label: 'Médicos',
      link: '/panel/doctors',
      faIcon: 'icon-medical'
    },
    {
      label: 'Administradores',
      link: '/panel/administrators',
      icon: 'verified_user'
    },
    {
      label: 'Famácia',
      faIcon: 'icon-pills',
      items: [
        {
          label: 'Famácias',
          link: '/panel/pharmacies',
          icon: 'storefront'
        },
        {
          label: 'Farmacêuticos',
          link: '/panel/pharmacists',
          icon: 'work_outline'
        },
        {
          label: 'Remédios',
          link: '/panel/medicines',
          faIcon: 'icon-pills'
        }
      ]
    }
  ];
  public menuConfig = {
    paddingAtStart: true,
    classname: 'primary-menu color-default',
    listBackgroundColor: 'transparent',
    fontColor: 'white',
    backgroundColor: '#55D3AE',
    selectedListFontColor: 'white',
    interfaceWithRoute: true,
    highlightOnSelect: false
  };
  public userProfile: any;
  constructor(private restService: RestService,
              public router: Router) {
              //   router.events.subscribe((val) => {
              //     if (val instanceof NavigationEnd) {
              //       this.configCurrentMenuItem(val);
              //     }
              // });
            }

  ngOnInit() {
  }

  ngAfterContentInit(): void {
    this.getLoggedUser();
  }

  /**
   * Verifica se o usuário está logado e recupera seus dados.
   */
  private getLoggedUser() {
    this.restService.getLoggedUser()
      .then((loggedInfo: string) => {
        if (loggedInfo) {
          this.userProfile = JSON.parse(loggedInfo);
        }
    });
  }

  /**
   * Realiza o loggout do usuário.
   */
  public loggout() {
    this.restService.doLoggout();
  }

  /**
   * Ao selecionar o item do menu.
   * @param $event item
   */
  public selectedMenuItem($event: any) {
    this.currentMenuItem = $event;
  }

  /**
   * configMenuItem
   */
  public configCurrentMenuItem(val: any) {
    this.itsOnDashboard = this.panel.test(val.url);
    Object.keys(this.childrenConfig).forEach(route => {
      if (val.urlAfterRedirects.search(route) !== -1) {
        this.menuConfig.backgroundColor = this.childrenConfig[route].color;
        this.menuConfig.classname = `primary-menu ${this.childrenConfig[route].class}`;
      }
    });
  }
}
