import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import localePtExtra from '@angular/common/locales/extra/pt';
registerLocaleData(localePt, 'pt-BR', localePtExtra);

import { PanelRoutingModule } from './panel-routing.module';
import { PanelComponent } from './panel.component';
import { RecipesComponent } from './../recipes/recipes.component';

// Tooltip
import { MatTooltipModule } from '@angular/material/tooltip';
import { CustomMaterialModule } from './../../../../core/material.module';
// Paginator
import { MatPaginatorModule } from '@angular/material';
// Formulário
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Select
import { MatSelectModule } from '@angular/material/select';
// Autocomplete
import { MatAutocompleteModule } from '@angular/material/autocomplete';
// Máscara de campos
import { NgxMaskModule } from 'ngx-mask';
// Expansivo
import { MatExpansionModule } from '@angular/material/expansion';
// Slider
import { MatSliderModule } from '@angular/material/slider';
// DatePicker
import { MatDatepickerModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
// Diretiva
import { MedicineItensComponent } from './../../../../directives/medicine-itens/medicine-itens.component';
import { RecipeCrudComponent } from './../recipes/recipe-crud/recipe-crud.component';
// ProgressBar
import { MatProgressBarModule } from '@angular/material/progress-bar';
// Custom Modules
import { CustomModulesModule } from './../../../../custom-modules/custom-modules.module';
@NgModule({
  declarations: [
    PanelComponent,
    RecipesComponent,
    MedicineItensComponent,
    RecipeCrudComponent
  ],
  imports: [
    CommonModule,
    PanelRoutingModule,
    CustomMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatProgressBarModule,
    MatSliderModule,
    MatDatepickerModule,
    MatInputModule,
    MatFormFieldModule,
    MatExpansionModule,
    CustomModulesModule,
    NgxMaskModule.forRoot()
  ],
  entryComponents: [RecipeCrudComponent],
  providers: [MatDatepickerModule,
    { provide: LOCALE_ID, useValue: 'pt-BR' }
  ]
})
export class PanelModule { }
