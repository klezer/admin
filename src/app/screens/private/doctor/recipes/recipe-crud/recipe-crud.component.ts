import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import * as moment from 'moment';
import { RestService } from './../../../../../services';
import { MatSnackBar } from '@angular/material';
import { environment } from './../../../../../../environments/environment';

@Component({
  selector: 'app-recipe-crud, [app-recipe-crud]',
  templateUrl: './recipe-crud.component.html',
  styleUrls: ['./recipe-crud.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class RecipeCrudComponent implements OnInit {
  public delayTimer: any;
  public moment = moment;
  public dosageList: any = [];
  public frequencyList: any = [];
  public routers = {
      getAnamnesePatient: 'doctor-get-anamnese-patient',
      searchCPF: 'doctor-search-user-cpf',
      createRecipe: 'doctor-create-recipe',
      downloadPDF: environment.apiUrl + '/api/patient-generate-recipe-pdf'
  };
  public formRecipe: any = [];
  public formToSumbit = {
    doctor_id: '',
    user_id: '',
    type_recipe_id: 1,
    medicines: ''
  };
  public anamneseInfo: any = [];
  public medicineList: any = [];
  public medicineItem: any = {
      dosage: null,
      considerations: null,
      frequency: null,
      number_of_days: null,
      title: null,
      subtitle: null,
      note: '',
      validity_of_medicine: null,
      opened: true
  };
  public canCreateRecipe = false;
  public patientProfile = {
    CPF: '',
    allProfile: null
  };
  public doctorID = null;
  public patientID = null;
  public urlPDF = null;
  public showPDF = false;

  constructor(public restService: RestService,
              public dialogRef: MatDialogRef<RecipeCrudComponent>,
              @Inject( MAT_DIALOG_DATA) public data: any,
              private snackBar: MatSnackBar) {
      if (this.data.recipeInfo) {
        this.doctorID = this.data.profileID;
        this.patientID = this.data.recipeInfo.medical_patient.id;
        this.formateAge(this.data.recipeInfo.medical_patient);
        this.getAnamneseInfo(this.patientID);
        this.getFileOfRecipe(this.data.recipeID);
      } else {
        this.userIsLogged();
      }
    }

    ngOnInit(): void {
      this.medicineList.push(this.medicineItem);
    }

    onNoClick(): void {
      this.dialogRef.close();
    }

  /**
   * Verifica se o usuário está logado.
   */
  public userIsLogged() {
    this.restService.getLoggedUser().then((loggedInfo: any) => {
      if (loggedInfo) {
        const userProfile = JSON.parse(loggedInfo);
        this.doctorID = userProfile.id;
      }
    });
  }

  /**
   * Formata a data de nascimento.
   */
  public formateAge(obj) {
    const ageFormated = obj.date_of_birth.split('/').reverse().join('-');
    return obj.date_of_birth = (new Date(ageFormated));
  }

  /**
   * Resgata as informações do paciente.
   */
  public getAnamneseInfo(patientID) {
      this.restService.getById(this.routers.getAnamnesePatient, patientID).then((response: any) => {
        this.anamneseInfo = response;
    });
  }

  /**
   * Adiciona o novo item do formulário validado.
   */
  public medicineForm(form) {
    if (form.new) {
        this.canCreateRecipe = true;
        this.formRecipe[form.index] = form.value;
        this.medicineList[form.index].medicine_id = form.value.medicine_id;
    }
    if (!form.new) {
        this.formRecipe.splice(form.index, 1);
        this.medicineList.splice(form.index, 1);
    }
  }

  /**
   * Valida o preenchimento do medicamento.
   */
  public validRecipe(valid) {
    this.canCreateRecipe = valid;
  }

  /**
   * Realiza a busca do CPF do Paciente.
   */
  public searchCPF() {
    this.restService.getAll(this.routers.searchCPF, this.patientProfile.CPF, ['error']).then((response: any) => {
      if (response.personal) {
        this.patientProfile.allProfile = response.personal;
        this.patientID = response.personal.user_id;
        this.formateAge(this.patientProfile.allProfile);
        this.getAnamneseInfo(this.patientID);
      }
    });
  }

  /**
   * Realiza a geração da receita.
   */
  public generateRecipe() {
     this.formToSumbit.doctor_id = this.doctorID;
     this.formToSumbit.user_id = this.patientID;
     this.formToSumbit.medicines = this.formRecipe;
     this.restService.postAll(this.routers.createRecipe, this.formToSumbit, false, ['error']).then((response: any) => {
        if (response.success) {
          this.dialogRef.close();
          this.snackBar.open('A receita gerada já está disponível.', '', {
            duration: 15000,
            panelClass: 'success'
          });
        }
     });
  }

  /**
   * Realiza o download do arquivo da receita.
   */
  public getFileOfRecipe(recipeID) {
    this.restService.getFile(this.routers.downloadPDF, recipeID).then((response: any) => {
      if (response.type === 'application/pdf') {
        this.urlPDF = URL.createObjectURL(response);
        this.showPDF = true;
      }
    });
  }

}
