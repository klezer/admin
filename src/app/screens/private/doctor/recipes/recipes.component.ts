import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { RecipeCrudComponent } from './recipe-crud/recipe-crud.component';
import { RestService, UtilsService, EventEmitterService } from './../../../../services';
import { environment } from './../../../../../environments/environment';
import { CustomDialogComponent } from './../../../../directives/custom-dialog/custom-dialog.component';

export interface RecipeObject {
  name: string;
  recipe_id: number;
  validity_of_recipe: string;
  date_generator: string;
  started_out: boolean;
  image: string;
}

export interface DialogData {
  animal: string;
  name: string;
}
@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.scss']
})
export class RecipesComponent implements OnInit {
  public userProfile: any;
  public myRoute: string;
  public loadingPDF = false;
  public routers: any = {
    retrieveAllRecipes: 'doctor-list-recipe',
    retrieveRecipeiD: 'doctor-recipe-patient',
    pdfUrl: environment.apiUrl + '/api/patient-generate-recipe-pdf/'
  };
  public allTables = {
    AllRecipes: {
      columns: [],
      info: {
        recipe_id: {
          title: 'ID'
        },
        name: {
          title: 'Nome',
          content: [
            { field: 'name', tag: 'h3' },
            { field: 'image', tag: 'img' }
          ],
        },
        date_generator: {
          title: 'Data de Emissão',
          date: 'dd/MM/y \'às\' H:mm\'h\''
        },
        validity_of_recipe: {
          title: 'Data de Validade',
          date: 'dd/MM/y'
        },
        started_out: {
          title: 'Andamento'
        },
        pdf: {
          title: 'PDF da Receita',
          icon: 'search',
          event: 'doctorLoadRecipePatient'
        }
      }
    }
  };
  constructor(public dialog: MatDialog,
              public utils: UtilsService,
              private restService: RestService) {
    this.myRoute = this.utils.getCurrentUrl();
    this.utils.resolveChildrenRoute(this.myRoute);
    this.getLoggedUser();
    this.getEvents();
  }

  ngOnInit() {
    this.defineRows();
  }

  /**
   * Verifica se o usuário está logado.
   */
  private getLoggedUser() {
    this.restService.getLoggedUser().then((loggedInfo: string) => {
      if (loggedInfo) {
        this.userProfile = JSON.parse(loggedInfo);
      }
    });
  }

  /**
   * Define as colunas da tabela.
   */
  private defineRows() {
    this.utils.defineRows(this.allTables);
  }

  // /**
  //  * Seleciona uma receita na tabela.
  //  */
  // public selectRecipe(item) {
  //   this.restService.getById(this.routers.retrieveRecipeiD, item.recipe_id).then((recipe: object) => {
  //     this.openRecipe(item.recipe_id, recipe);
  //   });
  // }

  /**
   * Abre o PopUp para adição de nova receita.
   */
  public newRecipe(): void {
    const dialogRef = this.dialog.open(RecipeCrudComponent, {
      width: '600px',
      panelClass: 'modal-component-screen',
      data: { profileID: this.userProfile.id, recipeInfo: null}
    });

    dialogRef.afterClosed().subscribe(result => {
      // this.getRecipes();
      console.dir(result);
    });
  }

  /**
   * Realiza o loggout do usuário.
   */
  public loggout() {
    this.restService.doLoggout();
  }

  /**
   * Exibe o modal de visualização do PDF.
   * @param route - Rota do recurso
   */
  private openRecipe(route: string) {
    let dialogData;
    let dialogRef;
    this.restService.getFile(route, null).then((response: any) => {
      if (response.type === 'application/pdf') {
        this.loadingPDF = !this.loadingPDF;
        dialogData = {
          width: '600px',
          panelClass: 'modal-component-screen',
          data: {
            title: 'Visualização da Receita',
            content: [
              {
                pdf: URL.createObjectURL(response)
              }
            ],
            cancelText: 'Cancelar',
            submitText: 'Fechar'
          }
        };
        dialogRef = this.dialog.open(CustomDialogComponent, dialogData);
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            this.loadingPDF = false;
          }
        });
      }
    });
  }

  /**
   * Pega os eventos dos componentes internos.
   */
  private getEvents() {
    EventEmitterService.get('doctorLoadRecipePatient').subscribe({
      next: data => {
        this.loadingPDF = !this.loadingPDF;
        this.openRecipe(data.route);
      }
    });
  }
}
