import { Component, OnInit, AfterContentInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { RestService } from './../../../../services';
import { element } from 'protractor';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterContentInit {
  public dashboard = /\/(dashboard)$/;
  public dashboardChild = /(\/dashboard\/)|(\/\d+)|(\/new)/g;
  public childrenRules = /([a-z])*.((\/\d+)|(new))/;
  public itsOnDashboard: boolean;
  public childrenComponents = [];
  public childrenConfig = {
    dashboard: { title: 'Dashboard', class: 'color-default', color: '#55D3AE', route: '/dashboard',
    icon: 'person_outline', personIcon: false },
    patients: { title: 'Paciente', class: 'color-patient', color: '#2196f3', route: '/dashboard/patients',
    icon: 'person_outline', personIcon: false  },
    doctors: { title: 'Médico', class: 'color-medical', color: '#009688', route: '/dashboard/doctors',
    icon: 'icon-medical', personIcon: true  },
    administrators: { title: 'Administrador', class: 'color-administrator', color: '#212121', route: '/dashboard/administrators',
    icon: 'verified_user', personIcon: false  },
    pharmacies: { title: 'Farmácia', class: 'color-pharmacie', color: '#673ab7', route: '/dashboard/pharmacies',
    icon: 'storefront', personIcon: false  },
    pharmacists: { title: 'Farmacêutico', class: 'color-pharmacist', color: '#ff5722', route: '/dashboard/pharmacists',
    icon: 'work_outline', personIcon: false },
    medicines: { title: 'Remédio', class: 'color-medicine', color: '#e4131b', route: '/dashboard/medicines',
    icon: 'icon-pills', personIcon: true  }
  };
  public myRoute = '/dashboard';
  public childRoute: string;
  public currentMenuItem = {
    label: 'Dashboard'
  };
  public childMenuItem = {
    label: 'Item',
    class: 'color-default'
  };
  public menuItems = [
    {
      label: 'Pacientes',
      link: '/dashboard/patients',
      icon: 'person_outline'
    },
    {
      label: 'Médicos',
      link: '/dashboard/doctors',
      faIcon: 'icon-medical'
    },
    {
      label: 'Administradores',
      link: '/dashboard/administrators',
      icon: 'verified_user'
    },
    {
      label: 'Famácia',
      faIcon: 'icon-pills',
      items: [
        {
          label: 'Famácias',
          link: '/dashboard/pharmacies',
          icon: 'storefront'
        },
        {
          label: 'Farmacêuticos',
          link: '/dashboard/pharmacists',
          icon: 'work_outline'
        },
        {
          label: 'Remédios',
          link: '/dashboard/medicines',
          faIcon: 'icon-pills'
        }
      ]
    }
  ];
  public menuConfig = {
    paddingAtStart: true,
    classname: 'primary-menu color-default',
    listBackgroundColor: 'transparent',
    fontColor: 'white',
    backgroundColor: '#55D3AE',
    selectedListFontColor: 'white',
    interfaceWithRoute: true,
    highlightOnSelect: false
  };
  public userProfile: any;
  constructor(private restService: RestService,
              public router: Router) {
              //   router.events.subscribe((val) => {
              //     if (val instanceof NavigationEnd) {
              //       this.configCurrentMenuItem(val);
              //     }
              // });
            }

  ngOnInit() {
    this.makeChildrenComponents();
  }

  ngAfterContentInit(): void {
    this.getLoggedUser();
  }

  /**
   * Verifica se o usuário está logado e recupera seus dados.
   */
  private getLoggedUser() {
    this.restService.getLoggedUser()
      .then((loggedInfo: string) => {
        if (loggedInfo) {
          this.userProfile = JSON.parse(loggedInfo);
        }
    });
  }

  /**
   * Realiza o loggout do usuário.
   */
  public loggout() {
    this.restService.doLoggout();
  }

  /**
   * Ao selecionar o item do menu.
   * @param $event item
   */
  public selectedMenuItem($event: any) {
    this.currentMenuItem = $event;
  }

  /**
   * configMenuItem
   */
  public configCurrentMenuItem(val: any) {
    this.itsOnDashboard = this.dashboard.test(val.url);
    Object.keys(this.childrenConfig).forEach(route => {
      if (val.urlAfterRedirects.search(route) !== -1) {
        this.menuConfig.backgroundColor = this.childrenConfig[route].color;
        this.menuConfig.classname = `primary-menu ${this.childrenConfig[route].class}`;
      }
    });
  }

  /**
   * Constroe os itens do módulo.
   */
  private makeChildrenComponents() {
    Object.keys(this.childrenConfig).forEach((item: any) => {
      this.childrenComponents.push(this.childrenConfig[item]);
    });
  }
}
