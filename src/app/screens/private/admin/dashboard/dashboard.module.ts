import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import localePtExtra from '@angular/common/locales/extra/pt';
registerLocaleData(localePt, 'pt-BR', localePtExtra);

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { PatientsComponent } from './../patients/patients.component';
import { PatientComponent } from './../patients/patient/patient.component';
import { DoctorsComponent } from './../doctors/doctors.component';
import { DoctorComponent } from './../doctors/doctor/doctor.component';
import { AdministratorsComponent } from './../administrators/administrators.component';
import { AdministratorComponent } from './../administrators/administrator/administrator.component';
import { PharmaciesComponent } from './../pharmacies/pharmacies.component';
import { PharmacyComponent } from './../pharmacies/pharmacy/pharmacy.component';
import { PharmacistsComponent } from './../pharmacists/pharmacists.component';
import { PharmacistComponent } from './../pharmacists/pharmacist/pharmacist.component';
import { MedicinesComponent } from './../medicines/medicines.component';
import { MedicineComponent } from './../medicines/medicine/medicine.component';
// Tooltip
import { MatTooltipModule } from '@angular/material/tooltip';
import { CustomMaterialModule } from './../../../../core/material.module';
// Formulário
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Select
import { MatSelectModule } from '@angular/material/select';
// Autocomplete
import { MatAutocompleteModule } from '@angular/material/autocomplete';
// Toggle
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// Máscara de campos
import { NgxMaskModule } from 'ngx-mask';
// Menu
import { NgMaterialMultilevelMenuModule } from 'ng-material-multilevel-menu';
// FlexLayout
import { FlexLayoutModule } from '@angular/flex-layout';
// Tabs
import { MatTabsModule } from '@angular/material/tabs';
// Dialog
import { MatDialogModule } from '@angular/material/dialog';
// ProgressBar
import { MatProgressBarModule } from '@angular/material/progress-bar';
// Diretivas
import { UserFormComponent } from './../../../../directives/user-form/user-form.component';
import { ResetLoginFormComponent } from './../../../../directives/reset-login-form/reset-login-form.component';
import { ListItemsComponent } from './../../../../directives/list-items/list-items.component';
import { MedicineFormComponent } from './../../../../directives/medicine-form/medicine-form.component';
// Custom Modules
import { CustomModulesModule } from './../../../../custom-modules/custom-modules.module';
@NgModule({
  declarations: [
    DashboardComponent,
    PatientsComponent,
    PatientComponent,
    DoctorsComponent,
    DoctorComponent,
    AdministratorsComponent,
    AdministratorComponent,
    PharmaciesComponent,
    PharmacyComponent,
    PharmacistsComponent,
    PharmacistComponent,
    MedicinesComponent,
    MedicineComponent,
    UserFormComponent,
    ResetLoginFormComponent,
    ListItemsComponent,
    MedicineFormComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    CustomMaterialModule,
    NgMaterialMultilevelMenuModule,
    MatTooltipModule,
    FlexLayoutModule,
    MatTabsModule,
    MatProgressBarModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatSlideToggleModule,
    MatDialogModule,
    CustomModulesModule,
    NgxMaskModule.forRoot()
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'pt-BR' }
  ]
})
export class DashboardModule { }
