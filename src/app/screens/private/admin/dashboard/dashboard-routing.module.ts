import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { PatientsComponent } from './../patients/patients.component';
import { PatientComponent } from './../patients/patient/patient.component';
import { DoctorsComponent } from './../doctors/doctors.component';
import { DoctorComponent } from './../doctors/doctor/doctor.component';
import { AdministratorsComponent } from './../administrators/administrators.component';
import { AdministratorComponent } from './../administrators/administrator/administrator.component';
import { PharmaciesComponent } from '../pharmacies/pharmacies.component';
import { PharmacyComponent } from './../pharmacies/pharmacy/pharmacy.component';
import { PharmacistsComponent } from './../pharmacists/pharmacists.component';
import { PharmacistComponent } from './../pharmacists/pharmacist/pharmacist.component';
import { MedicinesComponent } from './../medicines/medicines.component';
import { MedicineComponent } from './../medicines/medicine/medicine.component';


const routes: Routes = [
  { path: '', component: DashboardComponent, children: [
    { path: 'patients', component: PatientsComponent, children: [
      { path: ':id', component: PatientComponent }
      ]
    },
    { path: 'doctors', component: DoctorsComponent, children: [
      { path: ':id', component: DoctorComponent }
      ]
    },
    { path: 'administrators', component: AdministratorsComponent, children: [
      { path: ':id', component: AdministratorComponent }
      ]
    },
    { path: 'pharmacies', component: PharmaciesComponent, children: [
      { path: ':id', component: PharmacyComponent }
      ]
    },
    { path: 'pharmacists', component: PharmacistsComponent, children: [
      { path: ':id', component: PharmacistComponent }
      ]
    },
    { path: 'medicines', component: MedicinesComponent, children: [
      { path: ':id', component: MedicineComponent }
      ]
    }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
