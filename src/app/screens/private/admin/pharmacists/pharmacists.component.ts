import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestService, UtilsService } from './../../../../services';

@Component({
  selector: 'app-pharmacists',
  templateUrl: './pharmacists.component.html',
  styleUrls: ['./pharmacists.component.scss']
})

export class PharmacistsComponent implements OnInit {
  public userProfile: any;
  public childrenRules = /(\/(\d+)|\/(new))/;
  public myRoute: string;
  public routers = {
    retrieveAllPharmacists: 'get-all-pharmacists'
  };
  public allTables = {
    allPharmacists: {
      columns: [],
      info: {
        user_id: {
          title: 'ID'
        },
        name: {
          title: 'Nome',
          content: [
            { field: 'name', tag: 'h3' },
            { field: 'image', tag: 'img' }
          ],
        },
        cpf: {
          title: 'CPF',
          mask: '000.000.000-00'
        },
        fantasy_name: {
          title: 'Farmácia de Origem'
        },
        genre: {
          title: 'Gênero'
        },
      }
    }
  };

  constructor(private restService: RestService,
              public utils: UtilsService,
              public router: Router) {
    this.myRoute = this.utils.getCurrentUrl();
    this.utils.resolveChildrenRoute(this.myRoute);
    this.getLoggedUser();
  }

  ngOnInit() {
    this.defineRows();
  }

  /**
   * Verifica se o usuário está logado.
   */
  private getLoggedUser() {
    this.restService.getLoggedUser().then((loggedInfo: string) => {
      if (loggedInfo) {
        this.userProfile = JSON.parse(loggedInfo);
      }
    });
  }

  /**
   * Define as colunas da tabela.
   */
  private defineRows() {
    this.utils.defineRows(this.allTables);
  }
}
