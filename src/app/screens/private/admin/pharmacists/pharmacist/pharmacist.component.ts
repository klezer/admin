import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestService, UtilsService, EventEmitterService } from './../../../../../services';
import { CustomDialogComponent } from './../../../../../directives/custom-dialog/custom-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-pharmacist',
  templateUrl: './pharmacist.component.html',
  styleUrls: ['./pharmacist.component.scss']
})
export class PharmacistComponent implements OnInit {
  public tabIndex = 0;
  public rule = /(new)/;
  private ruleChildren = /(\/(\d+)|\/(new))/;
  private myRoute: string;
  public routers = {
    retrieveProfile: 'get-pharmacist-detail',
    createProfile: 'store-pharmacist',
    updateProfile: 'update-pharmacist',
    deleteProfile: 'delete-pharmacist',
    retrieveMyRecipes: 'get-sales-medicines-pharmacist',
    updateMyPassword: 'reset-send-password'
  };
  public loadingPDF = false;
  public currentProfile = {
    current_id: null,
    info: null
  };
  public allTables = {
    MyRecipes: {
      columns: [],
      info: {
        id: {
          title: 'ID'
        },
        title: {
          title: 'Medicamento'
        },
        patient_name: {
          title: 'Nome do Paciente',
          content: [
            { field: 'patient_name', tag: 'h3' },
            { field: 'image', tag: 'img' }
          ],
        },
        patient_cpf: {
          title: 'CPF',
          mask: '000.000.000-00'
        },
        fantasy_name: {
          title: 'Farmácia'
        },
        created_at: {
          title: 'Data da Venda',
          date: 'dd/MM/y \'às\' H:mm\'h\''
        },
        // pdf: {
        //   title: 'PDF da Receita',
        //   icon: 'search',
        //   event: 'pharmacistDownloadPDF'
        // }
      }
    }
  };
  constructor(private route: ActivatedRoute,
              private restService: RestService,
              public utils: UtilsService,
              private dialog: MatDialog) {
    this.restService.getLoggedUser();
    this.myRoute = this.utils.getCurrentUrl();
    this.getRouteParameters();
    this.getEvents();
  }

  ngOnInit() {
    this.retrieveProfile(this.currentProfile.current_id);
    this.defineRows();
  }

  /**
   * Recebe os parâmetros da rota.
   */
  public getRouteParameters() {
    this.route.paramMap.subscribe(params => {
      this.currentProfile.current_id = params.get('id');
    });
  }

  /**
   * Resgata os dados do perfil definido.
   * @param profileID - Identificação do perfil
   */
  public retrieveProfile(profileID: number) {
    if (!this.rule.test(this.currentProfile.current_id)) {
    this.restService.getById(this.routers.retrieveProfile, profileID)
      .then((response) => {
        this.currentProfile.info = response;
      });
    }
  }

  /**
   * Define as colunas da tabela.
   */
  private defineRows() {
    this.utils.defineRows(this.allTables);
  }

  /**
   * Exibe o modal de visualização do PDF.
   * @param route - Rota do recurso
   */
  private openRecipe(route: string) {
    let dialogData;
    let dialogRef;
    this.restService.getFile(route, null).then((response: any) => {
      if (response.type === 'application/pdf') {
        this.loadingPDF = !this.loadingPDF;
        dialogData = {
          width: '600px',
          panelClass: 'modal-component-screen',
          data: {
            title: 'Visualização da Receita',
            content: [
              {
                pdf: URL.createObjectURL(response)
              }
            ],
            cancelText: 'Cancelar',
            submitText: 'Fechar'
          }
        };
        dialogRef = this.dialog.open(CustomDialogComponent, dialogData);
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            // this.submitForm();
          }
        });
      }
    });
  }

  /**
   * Solicita a confirmação de remoção do perfil.
   */
  public confirmDeleteProfile() {
    const dialogRef = this.dialog.open(CustomDialogComponent, {
      width: '350px',
      panelClass: 'modal-component-screen',
      data: {
        title: 'Importante',
        content: [
          {
            title: 'Confirma remover o usuário?'
          }
        ],
        cancelText: 'Cancelar',
        submitText: 'Confirmar'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteProfile(this.currentProfile.current_id);
      }
    });
  }

  /**
   * Deleta o perfil.
   * @param profileID - Identificação do perfil
   */
  private deleteProfile(profileID: number) {
    this.restService.postAll(this.routers.deleteProfile, {user_id: profileID}, false, ['error', 'success'])
      .then((response: any) => {
        if (response.success) {
          this.utils.navigateTo(this.myRoute.replace(this.ruleChildren, ''));
        }
      });
  }

  /**
   * Pega os eventos dos componentes internos.
   */
  private getEvents() {
    EventEmitterService.get('downloadPDF').subscribe({
      next: data => {
        if (data.customRoute === this.routers.retrieveMyRecipes) {
          this.loadingPDF = !this.loadingPDF;
          this.openRecipe(data.route);
        }
      }
    });
    EventEmitterService.get('submitedUserForm').subscribe({
      next: () => {
        this.utils.navigateTo(this.myRoute.replace(this.ruleChildren, ''));
      }
    });
  }
}
