import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestService, UtilsService } from './../../../../services';

@Component({
  selector: 'app-medicines',
  templateUrl: './medicines.component.html',
  styleUrls: ['./medicines.component.scss']
})
export class MedicinesComponent implements OnInit {
  public childrenRules = /(\/(\d+)|\/(new))/;
  public userProfile: any;
  public myRoute: string;
  public routers = {
    retrieveAllMedicines: 'get-all-medicines'
  };
  public allTables = {
    allAdmins: {
      columns: [],
      info: {
        id: {
          title: 'ID'
        },
        titulo: {
          title: 'Título do medicamento',
          content: [
            { field: 'titulo', tag: 'h3' }
          ],
        },
        descricao: {
          title: 'Descrição'
        },
        tarja: {
          title: 'Tarja'
        },
        receituario: {
          title: 'Tipo'
        }
      }
    }
  };

  constructor(private restService: RestService,
              public utils: UtilsService,
              public router: Router) {
    this.myRoute = this.utils.getCurrentUrl();
    this.utils.resolveChildrenRoute(this.myRoute);
    this.getLoggedUser();
  }

  ngOnInit() {
    this.defineRows();
  }

  /**
   * Verifica se o usuário está logado.
   */
  private getLoggedUser() {
    this.restService.getLoggedUser().then((loggedInfo: string) => {
      if (loggedInfo) {
        this.userProfile = JSON.parse(loggedInfo);
      }
    });
  }

  /**
   * Define as colunas da tabela.
   */
  private defineRows() {
    this.utils.defineRows(this.allTables);
  }

}
