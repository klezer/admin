import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestService, UtilsService, EventEmitterService } from './../../../../../services';
import { CustomDialogComponent } from './../../../../../directives/custom-dialog/custom-dialog.component';
import { MatDialog } from '@angular/material/dialog';
@Component({
  selector: 'app-medicine',
  templateUrl: './medicine.component.html',
  styleUrls: ['./medicine.component.scss']
})
export class MedicineComponent implements OnInit {
  public tabIndex = 0;
  private rule = /(admin)/;
  private ruleChildren = /(\/(\d+)|\/(new))/;
  private myRoute: string;
  public routers = {
    createMedicine: 'store-medicines',
    retrieveMedicine: 'get-medicines-detail',
    updateMedicine: 'update-medicines',
    deleteMedicine: 'delete-medicines',
    updateMyPassword: 'password-reset',
  };
  public currentMedicine = {
    current_id: null,
    info: null
  };
  constructor(private route: ActivatedRoute,
              private restService: RestService,
              public utils: UtilsService,
              private dialog: MatDialog) {
    this.restService.getLoggedUser();
    this.myRoute = this.utils.getCurrentUrl();
    this.getRouteParameters();
    this.getEvents();
  }

  ngOnInit() {
    this.typeOfCrud();
  }

  /**
   * Recebe os parâmetros da rota.
   */
  public getRouteParameters() {
    this.route.paramMap.subscribe(params => {
      this.currentMedicine.current_id = params.get('id');
    });
  }

  /**
   * Define a regra de formulário de acordo com o
   */
  public typeOfCrud() {
    const validation = new RegExp(/\d/);
    if (validation.test(this.utils.getCurrentUrl())) {
      this.retrieveMedicine(this.currentMedicine.current_id);
    }
  }

  /**
   * Resgata os dados do registro.
   * @param medicineID - Identificação do medicamento
   */
  public retrieveMedicine(medicineID: number) {
    this.restService.getById(this.routers.retrieveMedicine, medicineID)
      .then((response) => {
        this.currentMedicine.info = response;
      });
  }

  /**
   * Solicita a confirmação de remoção do medicamento.
   */
  public confirmDeleteMedicine() {
    const dialogRef = this.dialog.open(CustomDialogComponent, {
      width: '350px',
      panelClass: 'modal-component-screen',
      data: {
        title: 'Importante',
        content: [
          {
            title: 'Confirma remover o medicamento?'
          }
        ],
        cancelText: 'Cancelar',
        submitText: 'Confirmar'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteMedicine(this.currentMedicine.current_id);
      }
    });
  }

  /**
   * Deleta o registro.
   * @param medicineID - Identificação do medicamento
   */
  private deleteMedicine(medicineID: number) {
    this.restService.postAll(this.routers.deleteMedicine, {medicine_id: medicineID}, false, ['error', 'success'])
      .then((response: any) => {
        if (response.success) {
          this.utils.navigateTo(this.myRoute.replace(this.ruleChildren, ''));
        }
      });
  }

  /**
   * Pega os eventos dos componentes internos.
   */
  private getEvents() {
    EventEmitterService.get('submitedMedicineForm').subscribe({
      next: () => {
        this.utils.navigateTo(this.myRoute.replace(this.ruleChildren, ''));
      }
    });
  }

}
