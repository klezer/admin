import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestService, UtilsService, EventEmitterService } from './../../../../../services';
import { CustomDialogComponent } from './../../../../../directives/custom-dialog/custom-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.scss']
})
export class PatientComponent implements OnInit {
  public tabIndex = 0;
  public rule = /(new)/;
  private ruleChildren = /(\/(\d+)|\/(new))/;
  private myRoute: string;
  public routers = {
    retrieveProfile: 'get-patient-detail',
    updateProfile: 'update-patient',
    deleteProfile: 'delete-patient',
    retrieveMyDoctors: 'get-all-doctor-patient',
    retrieveMyRecipes: 'get-all-recipes',
    retrieveInvites: 'get-all-invitation-user',
    updateMyPassword: 'password-reset'
  };
  public loadingPDF = false;
  public currentProfile = {
    current_id: null,
    info: null
  };
  public allTables = {
    myDoctors: {
      columns: [],
      info: {
        user_id: {
          title: 'ID'
        },
        name: {
          title: 'Nome',
          content: [
            { field: 'name', tag: 'h3' },
            { field: 'image', tag: 'img' }
          ],
        },
        cpf: {
          title: 'CPF',
          mask: '000.000.000-00'
        },
        genre: {
          title: 'Gênero'
        }
      }
    },
    MyRecipes: {
      columns: [],
      info: {
        recipe_id: {
          title: 'ID'
        },
        name: {
          title: 'Nome do Paciente',
          content: [
            { field: 'name', tag: 'h3' },
            { field: 'image', tag: 'img' }
          ],
        },
        created_at: {
          title: 'Emissão',
          date: 'ddd/MM/y \'às\' H:mm\'h\''
        },
        pdf: {
          title: 'PDF da Receita',
          icon: 'search',
          event: 'patientLoadRecipe'
        }
      }
    },
    MyInvites: {
      columns: [],
      info: {
        name: {
          title: 'Convidado',
          content: [
            { field: 'name', tag: 'h3' }
          ],
        },
        channel: {
          title: 'Canal'
        },
        value: {
          title: 'Dados'
        },
        status: {
          title: 'Status'
        }
      }
    }
  };
  constructor(private route: ActivatedRoute,
              private restService: RestService,
              public utils: UtilsService,
              private dialog: MatDialog) {
    this.restService.getLoggedUser();
    this.myRoute = this.utils.getCurrentUrl();
    this.getRouteParameters();
    this.getEvents();
  }

  ngOnInit() {
    this.retrieveProfile(this.currentProfile.current_id);
    this.defineRows();
  }

  /**
   * Recebe os parâmetros da rota.
   */
  public getRouteParameters() {
    this.route.paramMap.subscribe(params => {
      this.currentProfile.current_id = params.get('id');
    });
  }

  /**
   * Resgata os dados do perfil de acordo com o ID.
   */
  public retrieveProfile(profileID: number) {
    if (!this.rule.test(this.currentProfile.current_id)) {
    this.restService.getById(this.routers.retrieveProfile, profileID)
      .then((response) => {
        this.currentProfile.info = response;
      });
    }
  }

  /**
   * Define as colunas da tabela.
   */
  private defineRows() {
    this.utils.defineRows(this.allTables);
  }

  /**
   * Exibe o modal de visualização do PDF.
   * @param route - Rota do recurso
   */
  private openRecipe(route: string) {
    let dialogData;
    let dialogRef;
    this.restService.getFile(route, null).then((response: any) => {
      if (response.type === 'application/pdf') {
        this.loadingPDF = !this.loadingPDF;
        dialogData = {
          width: '600px',
          panelClass: 'modal-component-screen',
          data: {
            title: 'Visualização da Receita',
            content: [
              {
                pdf: URL.createObjectURL(response)
              }
            ],
            cancelText: 'Cancelar',
            submitText: 'Fechar'
          }
        };
        dialogRef = this.dialog.open(CustomDialogComponent, dialogData);
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            this.loadingPDF = false;
          }
        });
      }
    });
  }

  /**
   * Solicita a confirmação de remoção do usuário.
   */
  public confirmDeleteProfile() {
    const dialogRef = this.dialog.open(CustomDialogComponent, {
      width: '350px',
      panelClass: 'modal-component-screen',
      data: {
        title: 'Importante',
        content: [
          {
            title: 'Confirma remover o usuário?'
          }
        ],
        cancelText: 'Cancelar',
        submitText: 'Confirmar'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteProfile(this.currentProfile.current_id);
      }
    });
  }

  /**
   * Deleta o perfil.
   */
  private deleteProfile(profileID: number) {
    this.restService.postAll(this.routers.deleteProfile, {user_id: profileID}, false, ['error', 'success'])
      .then((response: any) => {
        if (response.success) {
          this.utils.navigateTo(this.myRoute.replace(this.ruleChildren, ''));
        }
      });
  }

  /**
   * Pega os eventos dos componentes internos.
   */
  private getEvents() {
    EventEmitterService.get('patientLoadRecipe').subscribe({
      next: data => {
        this.loadingPDF = !this.loadingPDF;
        this.openRecipe(data.route);
      }
    });
  }
}
