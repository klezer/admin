import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestService, UtilsService, EventEmitterService } from './../../../../../services';
import { CustomDialogComponent } from './../../../../../directives/custom-dialog/custom-dialog.component';
import { MatDialog } from '@angular/material/dialog';
@Component({
  selector: 'app-administrator',
  templateUrl: './administrator.component.html',
  styleUrls: ['./administrator.component.scss']
})
export class AdministratorComponent implements OnInit {
  public tabIndex = 0;
  private rule = /(admin)/;
  private ruleChildren = /(\/(\d+)|\/(new))/;
  private myRoute: string;
  public routers = {
    createProfile: 'store-admin',
    retrieveProfile: 'get-admin',
    updateProfile: 'update-admin',
    deleteProfile: 'delete-admin',
    updateMyPassword: 'password-reset',
  };
  public currentProfile = {
    current_id: null,
    info: null
  };
  constructor(private route: ActivatedRoute,
              private restService: RestService,
              public utils: UtilsService,
              private dialog: MatDialog) {
    this.restService.getLoggedUser();
    this.myRoute = this.utils.getCurrentUrl();
    this.getRouteParameters();
    this.getEvents();
  }

  ngOnInit() {
    this.typeOfCrud();
  }

  /**
   * Recebe os parâmetros da rota.
   */
  public getRouteParameters() {
    this.route.paramMap.subscribe(params => {
      this.currentProfile.current_id = params.get('id');
    });
  }

  /**
   * Define a regra de formulário de acordo com o
   */
  public typeOfCrud() {
    const validation = new RegExp(/\d/);
    if (validation.test(this.utils.getCurrentUrl())) {
      this.retrieveProfile(this.currentProfile.current_id);
    }
  }

  /**
   * Resgata os dados do perfil definido.
   * @param profileID - Identificação do perfil
   */
  public retrieveProfile(profileID: number) {
    this.restService.getById(this.routers.retrieveProfile, profileID)
      .then((response) => {
        this.currentProfile.info = response;
      });
  }

  /**
   * Solicita a confirmação de remoção do usuário.
   */
  public confirmDeleteProfile() {
    const dialogRef = this.dialog.open(CustomDialogComponent, {
      width: '350px',
      panelClass: 'modal-component-screen',
      data: {
        title: 'Importante',
        content: [
          {
            title: 'Confirma remover o usuário?'
          }
        ],
        cancelText: 'Cancelar',
        submitText: 'Confirmar'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteProfile(this.currentProfile.current_id);
      }
    });
  }

  /**
   * Deleta o perfil.
   * @param profileID - Identificação do perfil
   */
  private deleteProfile(profileID: number) {
    this.restService.postAll(this.routers.deleteProfile, {user_id: profileID}, false, ['error', 'success'])
      .then((response: any) => {
        if (response.success) {
          this.utils.navigateTo(this.myRoute.replace(this.ruleChildren, ''));
        }
      });
  }

  /**
   * Pega os eventos dos componentes internos.
   */
  private getEvents() {
    EventEmitterService.get('submitedUserForm').subscribe({
      next: () => {
        this.utils.navigateTo(this.myRoute.replace(this.ruleChildren, ''));
      }
    });
  }

}
