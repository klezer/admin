import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestService, UtilsService, EventEmitterService } from './../../../../../services';
import { CustomDialogComponent } from './../../../../../directives/custom-dialog/custom-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-pharmacy',
  templateUrl: './pharmacy.component.html',
  styleUrls: ['./pharmacy.component.scss']
})

export class PharmacyComponent implements OnInit {
  public tabIndex = 0;
  public rule = /(new)/;
  private ruleChildren = /(\/(\d+)|\/(new))/;
  private myRoute: string;
  public routers = {
    createProfile: 'store-pharmacy',
    retrieveProfile: 'get-pharmacy-detail',
    updateProfile: 'update-pharmacy',
    deleteProfile: 'delete-pharmacy',
    retrieveMyFarmacists: 'get-all-pharmacist',
    updateMyPassword: 'reset-send-password'
  };
  public loadingPDF = false;
  public currentProfile = {
    current_id: null,
    info: null
  };
  public allTables = {
    myPatients: {
      columns: [],
      info: {
        user_id: {
          title: 'ID'
        },
        name: {
          title: 'Nome',
          content: [
            { field: 'name', tag: 'h3' },
            { field: 'image', tag: 'img' }
          ],
        },
        cpf: {
          title: 'CPF',
          mask: '000.000.000-00'
        },
        email: {
          title: 'E-mail'
        },
        genre: {
          title: 'Gênero'
        }
      }
    },
    MyRecipes: {
      columns: [],
      info: {
        recipe_id: {
          title: 'ID'
        },
        name: {
          title: 'Nome do Paciente',
          content: [
            { field: 'name', tag: 'h3' },
            { field: 'image', tag: 'img' }
          ],
        },
        created_at: {
          title: 'Emissão',
          date: 'dd/MM/y \'às\' H:mm\'h\''
        },
        pdf: {
          title: 'PDF da Receita',
          icon: 'search',
          event: 'pharmacyDownloadPDF'
        }
      }
    }
  };
  constructor(private route: ActivatedRoute,
              private restService: RestService,
              public utils: UtilsService,
              private dialog: MatDialog) {
    this.restService.getLoggedUser();
    this.myRoute = this.utils.getCurrentUrl();
    this.getRouteParameters();
    this.getEvents();
  }

  ngOnInit() {
    this.retrieveProfile(this.currentProfile.current_id);
    this.defineRows();
  }

  /**
   * Recebe os parâmetros da rota.
   */
  public getRouteParameters() {
    this.route.paramMap.subscribe(params => {
      this.currentProfile.current_id = params.get('id');
    });
  }

  /**
   * Resgata os dados do perfil.
   * @param profileID - Identificação do perfil
   */
  public retrieveProfile(profileID: number) {
    if (!this.rule.test(this.currentProfile.current_id)) {
    this.restService.getById(this.routers.retrieveProfile, profileID)
      .then((response) => {
        this.currentProfile.info = response;
      });
    }
  }

  /**
   * Define as colunas da tabela.
   */
  private defineRows() {
    this.utils.defineRows(this.allTables);
  }

  /**
   * Solicita a confirmação de remoção do usuário.
   */
  public confirmDeleteProfile() {
    const dialogRef = this.dialog.open(CustomDialogComponent, {
      width: '350px',
      panelClass: 'modal-component-screen',
      data: {
        title: 'Importante',
        content: [
          {
            title: 'Confirma remover o usuário?'
          }
        ],
        cancelText: 'Cancelar',
        submitText: 'Confirmar'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteProfile(this.currentProfile.current_id);
      }
    });
  }

  /**
   * Deleta o perfil.
   * @param profileID - Identificação do perfil
   */
  private deleteProfile(profileID: number) {
    this.restService.postAll(this.routers.deleteProfile, {user_id: profileID}, false, ['error', 'success'])
      .then((response: any) => {
        if (response.success) {
          this.utils.navigateTo(this.myRoute.replace(this.ruleChildren, ''));
        }
      });
  }

  /**
   * Pega os eventos dos componentes internos.
   */
  private getEvents() {
    EventEmitterService.get('submitedUserForm').subscribe({
      next: () => {
        this.utils.navigateTo(this.myRoute.replace(this.ruleChildren, ''));
      }
    });
  }
}
