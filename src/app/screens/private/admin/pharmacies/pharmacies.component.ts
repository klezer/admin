import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestService, UtilsService } from './../../../../services';
@Component({
  selector: 'app-pharmacies',
  templateUrl: './pharmacies.component.html',
  styleUrls: ['./pharmacies.component.scss']
})
export class PharmaciesComponent implements OnInit {
  public userProfile: any;
  public childrenRules = /(\/(\d+)|\/(new))/;
  public myRoute: string;
  public routers = {
    retrieveAllPharmacies: 'get-all-pharmacies'
  };
  public allTables = {
    allPharmacies: {
      columns: [],
      info: {
        user_id: {
          title: 'ID'
        },
        fantasy_name: {
          title: 'Nome Fantasia',
          content: [
            { field: 'fantasy_name', tag: 'h3' },
          ],
        },
        cnpj: {
          title: 'CNPJ',
          mask: '00.000.000/000-00'
        },
        responsible: {
          title: 'Responsável'
        },
        email: {
          title: 'E-mail'
        },
      }
    }
  };

  constructor(private restService: RestService,
              public utils: UtilsService,
              public router: Router) {
    this.myRoute = this.utils.getCurrentUrl();
    this.utils.resolveChildrenRoute(this.myRoute);
    this.getLoggedUser();
  }

  ngOnInit() {
    this.defineRows();
  }

  /**
   * Verifica se o usuário está logado.
   */
  private getLoggedUser() {
    this.restService.getLoggedUser().then((loggedInfo: string) => {
      if (loggedInfo) {
        this.userProfile = JSON.parse(loggedInfo);
      }
    });
  }

  /**
   * Define as colunas da tabela.
   */
  private defineRows() {
    this.utils.defineRows(this.allTables);
  }

}
