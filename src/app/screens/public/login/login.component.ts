import { Component, HostBinding } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { RestService } from './../../../services';

/** Erro quando o controle inválido está sujo, é tocado ou enviado. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [{provide: ErrorStateMatcher}]
})
export class LoginComponent {
  @HostBinding('class') public cssClass = 'screen-login';
  public loginForm: any;
  public matcher = new MyErrorStateMatcher();
  public hide = true;
  public formType = 'login';
  private routers = {
    signIn: 'api-login',
    recoveryPassword: 'send-email-reset',
    codeValidation: 'validate-code-reset',
    newPassword: 'password-reset'
  };
  constructor(private router: Router,
              private restService: RestService) {
    this.getLoggedUser();
    this.formData('login');
  }

  /**
   * Verifica se o usuário está logado e se estiver o encaminha para o rota.
   */
  private getLoggedUser() {
    this.restService.getLoggedUser().then((logged: any) => {
      if (logged) {
        this.resolveModule(JSON.parse(logged));
      }
    });
  }

/**
 * Define os campos e regras do formulário.
 * @param type - Tipo de formulário
 */
  public formData(type) {
    const loginForm = {
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl(''),
      code: new FormControl(''),
      user_id: new FormControl('')
    };

    if (type === 'login') {
      loginForm.password = new FormControl('', [
        Validators.required, Validators.minLength(6)
      ]);
    } else if (type === 'forggot') {
      loginForm.email = new FormControl('', [
        Validators.required,
        Validators.email
      ]);
    } else if (type === 'code') {
      loginForm.email = new FormControl('');
      loginForm.code = new FormControl('', [
        Validators.required, Validators.minLength(5)
      ]);
    } else if (type === 'password') {
      loginForm.email = new FormControl('');
      loginForm.password = new FormControl('', [
        Validators.required, Validators.minLength(6)
      ]);
    }
    this.formType = type;
    this.loginForm  = new FormGroup(loginForm);
  }

  /**
   * Submete os dados do formuário de login.
   */
  public submitLogin() {
    this.restService.postAll(this.routers.signIn, this.loginForm.value, true, ['error'])
      .then((response: any) => {
        this.restService.setUserProfile(JSON.stringify(response.success)).then((response));
        if (response.success) {
          this.restService.setApiServiceTokenApi(response.success.token);
          this.resolveModule(response.success);
      }
    });
  }

  /**
   * Encaminha o usuário para o múdulo específico.
   * @param user - Dados do usuário
   */
  private resolveModule(user) {
    if (user.is_admin) {
      this.router.navigate(['/dashboard']);
    }
    if (user.is_doctor) {
      this.router.navigate(['/panel']);
    }
  }

  /**
   * Submete o email para a recuperação de senha.
   */
  public recoveryPassword() {
    this.restService.postAll(this.routers.recoveryPassword, { email: this.loginForm.get('email').value }, false, ['error', 'success'])
      .then((response: any) => {
      if (response.success) {
        this.formData('code');
        localStorage.setItem('recoveryID', response.success.user_id);
      }
    });
  }

  /**
   * Submete o código para validação.
   */
  public validateRecoveryCode() {
    const form = {
      code: this.loginForm.get('code').value,
      user_id: localStorage.getItem('recoveryID')
    };

    this.restService.postAll(this.routers.codeValidation, form, false, ['error', 'success'])
      .then((response: any) => {
      if (response.success) {
        this.formData('password');
      }
    });
  }

  /**
   * Submete a nova senha.
   */
  public defineNewPassword() {
    const form = {
      password: this.loginForm.get('password').value,
      user_id: localStorage.getItem('recoveryID')
    };

    this.restService.postAll(this.routers.newPassword, form, false, ['error', 'success'])
      .then((response: any) => {
      if (response.success) {
        this.formData('login');
      }
    });
  }
}
