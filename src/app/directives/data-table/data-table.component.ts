import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { RestService, EventEmitterService } from './../../services';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-data-table, [app-data-table]',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class DataTableComponent implements OnInit {
  public dataTable = new MatTableDataSource<string>();
  public currentPage = 1;
  public perPage = 10;
  public loadedAsync = false;
  private delayTimer: any;
  public count: number;
  public abortedRequest = false;
  public expandedElement = null;

  @Input() private expansibleRule?: string;
  @Input() private customRoute: string;
  @Input() public loadingResources?: boolean;
  @Input() public userID?: any;
  @Input() public displayedColumns: any;
  @Input() public displayedColumnsInfo?: any;
  @Output() private customRowClick?: EventEmitter<any> = new EventEmitter();
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private restService: RestService,
              public router: Router) {
    this.getEvents();
  }

  ngOnInit() {
    this.dataTable.paginator = this.paginator;
    this.userID = (this.userID) ? this.userID : false;
    this.getAll(false);
  }

  /**
   * Raliza a busca com ou sem busca de termo.
   * @param search - Parâmetro de busca
   */
  public getAll(search: boolean|string) {
    this.restService
      .getAllPaginate(
        this.customRoute,
        this.userID,
        this.currentPage,
        ['error'],
        search,
        this.perPage
      )
      .then((response: any) => {
        this.loadedAsync = true;
        this.count = response.total;
        this.dataTable = new MatTableDataSource<string>(response.data);
      });
  }

  /**
   * Realiza a consulta do termo desejado.
   * @param searchInfo - Termo desejado
   */
  public search(searchInfo: string) {
    this.loadedAsync = false;
    this.currentPage = 1;
    this.dataTable = new MatTableDataSource<string>([]);
    this.getAll(searchInfo.length > 0 ? searchInfo : false);
  }

  /**
   * Seleciona uma linha na tabela.
   * @param row - Linha
   */
  public selectRow(row: any) {
    if (this.expansibleRule &&
        this.expansibleRule.length > 0) {
      const ruleDefinition: any = this.processExpansibleRule(row);
      // tslint:disable-next-line:no-eval
      const rule = eval(ruleDefinition);
      if (rule) {
        this.expandedElement = this.expandedElement === row ? null : row;
      } else {
        this.customRowClick.emit(row);
      }
    } else {
      this.customRowClick.emit(row);
    }
  }

  /**
   * processExpansibleRule
   */
  public processExpansibleRule(row) {
    const rule = this.expansibleRule.split(' ');
    const rules = {
      field: rule[0].replace(/(')/g, ''),
      oparator: rule[1],
      require: rule[2]
    };
    return `'${row[rules.field]}' ${rules.oparator} ${rules.require}`;
  }

  /**
   * Emite evento ao clicar na coluna.
   * @param event - Objeto do evento
   * @param info - Objeto da linha
   */
  public clickColumn(event: any, route: string) {
   this.loadPDF(event, route);
  }

  /**
   * Carrega o PDF da receita através de um evento.
   * @param event - Nome do evento
   * @param route - Rota do recurso
   */
  public loadPDF(event: any, route: string) {
    const data = {
      route,
      origin,
      customRoute: this.customRoute
    };
    EventEmitterService.get(event).emit(data);
  }

  /**
   * Realiza o filtro do termo desejado.
   * @param filterValue - Termo desejado.
   */
  public applyFilter(filterValue: string) {
    if (this.dataTable.filteredData.length === 0) {
      clearTimeout(this.delayTimer);
      this.delayTimer = setTimeout(() => {
        this.search(filterValue);
      }, 500);
    } else {
      this.dataTable.filter = filterValue.trim().toLowerCase();
    }
  }

  /**
   * Pega o status da paginação.
   * @param $event - Objeto da paginação
   */
  public getPagination($event: any) {
    this.currentPage = $event.pageIndex + 1;
    this.perPage = $event.pageSize;
    this.getAll(false);
    console.dir($event);
  // length: 0
  // pageIndex: 0
  // pageSize: 50
  // previousPageIndex: 0
  }

  /**
   * Pega os eventos dos componentes internos.
   */
  private getEvents() {
    EventEmitterService.get('abortedRequest').subscribe({
      next: (data: boolean) => {
        this.abortedRequest = data;
      }
    });
  }

}
