import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { FormControl, FormGroup, FormGroupDirective, NgForm, Validators, ValidationErrors } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { RestService, EventEmitterService } from './../../services';
import { isArray } from 'util';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

/** Erro quando o controle inválido está sujo, é tocado ou enviado. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

export interface Select {
  value: any;
  display: string;
}

@Component({
  selector: 'app-medicine-form, [app-medicine-form]',
  templateUrl: './medicine-form.component.html',
  styleUrls: ['./medicine-form.component.scss'],
  providers: [{provide: ErrorStateMatcher}]
})
export class MedicineFormComponent implements OnInit, OnChanges {
  public medicineForm: FormGroup;
  public matcher = new MyErrorStateMatcher();
  private routers = {
    retrieveManufacturers: 'combo-manufacturers',
    retrieveTypes: 'combo-types'
  };
  public tarjetas: Select[] = [
    {value: 'A', display: 'Amarela'},
    {value: 'P', display: 'Preta'},
    {value: 'V', display: 'Vermelha'}
  ];
  public manufacturers: Select[];
  public types: Select[];
  public manufacturersFilteredOptions: Observable<Select[]>;
  public typesFilteredOptions: Observable<Select[]>;
  public finalRouteToPost: string;
  private rule = /\d/;
  public formSubmitted = false;
  private autocompleFields = {
    manufacturers: 'manufacturer_id',
    types: 'type_id'
  };

  @Input() public medicineID?: any;
  @Input() public dataForm?: any;
  @Input() private routeToCreate?: string;
  @Input() private routeToUpdate?: string;
  @Input() private serialized = false;

  constructor(private restService: RestService) {
    this.formData();
    this.restService.getLoggedUser();
  }

  ngOnInit() {
    this.finalRouteToPost = (this.rule.test(this.medicineID)) ? this.routeToUpdate : this.routeToCreate;
  }

  ngOnChanges(changes: any) {
    if (changes.dataForm.currentValue) {
      this.formData();
      this.getSelectList(this.routers);
    }
  }

  /**
   * Carrega os combos padrão.
   * @param allRouter - Lista de rotas de bsca
   */
  public getSelectList(allRouter: any) {
    Object.keys(allRouter).forEach(route => {
      const list = route.replace('retrieve', '').toLowerCase();
      const listField = `${list}_title`;
      const field = listField.replace('s_', '_');
      if (this.dataForm && this.dataForm[field]) {
        this.loadSelectListPaginate(list, allRouter[route], this.dataForm[field]);
      } else {
        this.loadSelectList(list, allRouter[route]);
      }
    });
  }

  /**
   * Busca os dados dos combos.
   * @param list - Lista que deverá ser alimentada
   * @param route - Rota de busca dos objetos para a lista
   */
  public loadSelectList(list: any, route: string) {
    this.restService.getAll(route, null, ['error'])
      .then((response: Select[])  => {
        this[list] = response;
        this.allowAutoComplete(list);
      });
  }

  /**
   * Busca os dados dos combos.
   * @param list - Lista que deverá ser alimentada
   * @param route - Rota de busca dos objetos para a lista
   * @param search - Parâmetro opcional da busca
   */
  public loadSelectListPaginate(list: any, route: string, search = null) {
    this.restService.getAllPaginate(route, false, 1, ['error'], search)
      .then((response: Select[])  => {
        this[list] = response;
        this.allowAutoComplete(list);
      });
  }

  /**
   * Define os campos e regras do formulário.
   */
  public formData() {
    const medicineForm = {
      id: new FormControl(''),
      title: new FormControl('', [Validators.required]),
      subtitle: new FormControl('', [Validators.required]),
      note: new FormControl('', [Validators.required]),
      manufacturer_id: new FormControl('', [Validators.required]),
      type_id: new FormControl('', [Validators.required]),
      price: new FormControl('', [Validators.required]),
      maximum_price: new FormControl('', [Validators.required]),
      minimum_price: new FormControl('', [Validators.required])
    };

    if (this.rule.test(this.medicineID)) {
      medicineForm.id = new FormControl(this.medicineID);
    }

    this.medicineForm = new FormGroup(medicineForm);

    this.retrieveData(this.dataForm, this.medicineForm);
  }

  /**
   * Insere os dados no formulário.
   */
  public retrieveData(data: any, form: FormGroup) {
    if (this.dataForm && this.rule.test(this.medicineID)) {
      const list = isArray(data) ? data : Object.keys(data);
      list.forEach(field => {
        if (form.controls[field] && (field.length > 0)) {
          form.controls[field].setValue(data[field]);
        }
        if (isArray(data[field])) {
          data[field].forEach(children => {
            this.retrieveData(children, form);
          });
        }
      });
    }
  }

  /**
   * Submete os dados do formulário.
   */
  public submitForm() {
    if (this.medicineForm.valid) {
      this.formSubmitted = !this.formSubmitted;
      this.restService.postAll(
        this.finalRouteToPost,
        this.medicineForm.value,
        this.serialized,
        ['success', 'error']
      ).then((response: any) => {
        this.formSubmitted = !this.formSubmitted;
        if (response.success) {
          EventEmitterService.get('submitedMedicineForm').emit({type: this.finalRouteToPost, response});
        }
      });
    }
  }

  /**
   * Permite a busca de campos com autocomplete.
   * @param list - Lista de carregamento de opções do autocomplete
   * @param field - Campo do autocomplete
   */
  private allowAutoComplete(list: any) {
    if (this.autocompleFields[list] &&
        this[list] && this[list].length > 0) {
      const field = this.autocompleFields[list];
      this[`${list}FilteredOptions`] = this.medicineForm.controls[field].valueChanges
      .pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : value.display),
        map(value => value ? this._filter(value, list) : this[list].slice())
      );
    }
  }

  /**
   * Monitora a alteração de valor do autocomplete.
   * @param list - Lista da comparação
   */
  public displayFn(list: Select[]): (id: number) => string {
    return (value: number) => {
      const correspondingOption = Array.isArray(list) ? list.find(option => option.value === value) : null;
      return correspondingOption ? correspondingOption.display : '';
    };
  }

  /**
   * Realiza o filtro do valor do autocomplete.
   * @param display - campo de exibição
   * @param list - Lista de busca do campo
   */
  private _filter(display: string, list: any): Select[] {
    const filterValue = display.toLowerCase();
    const result = this[list].filter(option => option.display.toLowerCase().indexOf(filterValue) === 0);
    if (result.length > 0) {
      return result;
    } else if (this.autocompleFields[list]) {
      this.autoSearchOnDemand(filterValue, list).then((response) => {
        return response;
      });
    }
  }

  /**
   * Realiza o filtro do valor do autocomplete sob demanda
   * caso já não exista na lista.
   * @param search - Termo da busca
   * @param list - Lista de busca de campo
   */
  private autoSearchOnDemand(search: string, list: any) {
    const route = `retrieve${list.charAt(0).toUpperCase() + list.slice(1)}`;
    return new Promise((resolve) => {
      this.restService.getAllPaginate(this.routers[route], null, 1, ['error'], search).then((response: any) => {
        this[list] = response;
        resolve(response);
      });
    });
  }

  /**
   * Exibe os campos que estão com erro na validação.
   */
  private getFormValidationErrors() {
    const result = [];
    Object.keys(this.medicineForm.controls).forEach(key => {
      const controlErrors: ValidationErrors = this.medicineForm.get(key).errors;
      if (controlErrors) {
        Object.keys(controlErrors).forEach(keyError => {
          result.push({
            control: key,
            error: keyError,
            value: controlErrors[keyError]
          });
        });
      }
    });
    console.dir(result);
    return result;
  }

}
