import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { RestService, EventEmitterService } from './../../services';
import { CustomDialogComponent } from './../custom-dialog/custom-dialog.component';
import { MatDialog } from '@angular/material/dialog';

/** Erro quando o controle inválido está sujo, é tocado ou enviado. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-reset-login-form, [app-reset-login-form]',
  templateUrl: './reset-login-form.component.html',
  styleUrls: ['./reset-login-form.component.scss'],
  providers: [{provide: ErrorStateMatcher}]
})

export class ResetLoginFormComponent implements OnInit {
  public resetLoginForm: any;
  public matcher = new MyErrorStateMatcher();
  public hide = true;
  public changePassword = false;
  public formSubmitted = false;
  @Input() public userID?: any;
  @Input() private routeToUpdate?: string;
  @Input() private serialized = false;

  constructor(private restService: RestService,
              private dialog: MatDialog) {
    this.formData();
    this.restService.getLoggedUser();
  }

  ngOnInit() {
  }

  /**
   * Define os campos e regras do formulário.
   */
  public formData() {
    const resetLoginForm = {
      user_id: new FormControl(this.userID),
      password: new FormControl('', [Validators.required, Validators.minLength(6)])
    };

    this.resetLoginForm = new FormGroup(resetLoginForm);

  }

  /**
   * Realiza a alteração do componente mat-slide-toggle.
   * @param $event - Objeto do evento
   */
  public changeValue($event: any) {
    this.changePassword = $event.checked;
    if (this.changePassword) {
      this.formData();
    }
  }

  /**
   * Exibe o modal de confirmação da senha.
   */
  public confirmChange() {
    const dialogRef = this.dialog.open(CustomDialogComponent, {
      width: '350px',
      panelClass: 'modal-component-screen',
      data: {
        title: 'Importante',
        content: [
          {
            title: 'Confirma a alteração de senha?'
          }
        ],
        cancelText: 'Cancelar',
        submitText: 'Confirmar'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.submitForm();
      }
    });
  }

  /**
   * Submete os dados do formulário.
   */
  public submitForm() {
    if (this.resetLoginForm.valid) {
        this.formSubmitted = !this.formSubmitted;
        this.restService.postAll(
        this.routeToUpdate,
        this.resetLoginForm.value,
        this.serialized,
        ['error', 'success']
      ).then((res: any) => {
        this.formSubmitted = !this.formSubmitted;
        if (res.success) {
          this.resetLoginForm.reset();
          this.changePassword = false;
          EventEmitterService.get('submitedUserForm').emit(res);
        }
      });
    }
  }

}
