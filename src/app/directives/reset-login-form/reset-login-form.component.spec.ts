import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetLoginFormComponent } from './reset-login-form.component';

describe('ResetLoginFormComponent', () => {
  let component: ResetLoginFormComponent;
  let fixture: ComponentFixture<ResetLoginFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetLoginFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetLoginFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
