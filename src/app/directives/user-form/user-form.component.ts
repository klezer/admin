import { Component, OnInit, Input, ViewChild, AfterViewInit, OnChanges } from '@angular/core';
import { FormControl, FormGroup, FormGroupDirective, NgForm, Validators, ValidationErrors } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { RestService, EventEmitterService } from './../../services';
import { isArray } from 'util';
import { MatSelect } from '@angular/material/select';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

/** Erro quando o controle inválido está sujo, é tocado ou enviado. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

export interface Select {
  value: any;
  display: string;
}

@Component({
  selector: 'app-user-form, [app-user-form]',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss'],
  providers: [{provide: ErrorStateMatcher}]
})
export class UserFormComponent implements AfterViewInit, OnInit, OnChanges {
  public dinamicForm: FormGroup;
  public matcher = new MyErrorStateMatcher();
  public routers = {
    retrieveStates: 'combo-states',
    retrieveAdvices: 'combo-advices',
    retrievePharmacies: 'combo-pharmacies',
    retrieveCitys: null
  };
  private autoLoadSelect = {
    selectState: 'citys',
    selectAdvice: 'specialties'
  };
  public genres: Select[] = [
    {value: 'M', display: 'Masculino'},
    {value: 'F', display: 'Feminino'}
  ];
  public advices: Select[];
  public regions: Select[];
  public states: Select[];
  public citys: Select[];
  public pharmacies: Select[];
  public specialties: Select[];
  public pharmaciesFilteredOptions: Observable<Select[]>;
  public citysFilteredOptions: Observable<Select[]>;
  public finalRouteToPost: string;
  private rule = /\d/;
  public formSubmitted = false;
  private autocompleFields = {
    pharmacies: 'pharmacy_id',
    citys: 'city_id'
  };

  @Input() public type: string;
  @Input() public userID?: any;
  @Input() public dataForm?: any;
  @Input() private routeToCreate?: string;
  @Input() private routeToUpdate?: string;
  @Input() private serialized = false;

  @ViewChild('state', { static: false }) public selectState: MatSelect;
  @ViewChild('advice', { static: false }) public selectAdvice: MatSelect;

  constructor(private restService: RestService) {
    this.formData(false);
    this.restService.getLoggedUser();
  }

  ngOnInit() {
    this.finalRouteToPost = (this.rule.test(this.userID)) ? this.routeToUpdate : this.routeToCreate;
  }

  ngAfterViewInit() {
    this.loadSpecificSelectListAuto();
  }

  ngOnChanges() {
    this.formData(this.type ? this.type : false);
    this.getSelectList(this.routers);
  }

  /**
   * Define os campos e regras do formulário.
   */
  public formData(type: boolean|string) {
    console.log(type);
    const dinamicForm = {
      user_id: new FormControl(''),
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      telephone_one: new FormControl('', [Validators.required]),
      cpf: new FormControl('', [Validators.required]),
      rg: new FormControl('', [Validators.required]),
      street_code: new FormControl('', [Validators.required, Validators.minLength(8)]),
      street_title: new FormControl('', [Validators.required]),
      number: new FormControl('', [Validators.required]),
      state_id: new FormControl('', [Validators.required]),
      complement: new FormControl(''),
      city_id: new FormControl('', [Validators.required]),
      city_title: new FormControl('Afonso Cláudio'),
      district_title: new FormControl('', [Validators.required]),
      genre: new FormControl('', [Validators.required]),
      document_professional: new FormControl('upload', [Validators.required]),
      date_of_birth: new FormControl('', [Validators.required]),
      region_id: new FormControl(''),
      advice_id: new FormControl(''),
      specialty_id: new FormControl(''),
      crm_number: new FormControl(''),
      locality: new FormControl(''),
      cnpj: new FormControl(''),
      fantasy_name: new FormControl(''),
      responsible: new FormControl(''),
      pharmacy_id: new FormControl('')
    };

    if (type === 'doctor') {
      dinamicForm.region_id = new FormControl('', [Validators.required]);
      dinamicForm.advice_id = new FormControl('', [Validators.required]);
      dinamicForm.specialty_id = new FormControl('', [Validators.required]);
      dinamicForm.crm_number = new FormControl('', [Validators.required]);
      dinamicForm.locality = new FormControl('', [Validators.required]);
    } else if (type === 'pharmacy') {
      dinamicForm.cnpj = new FormControl('', [Validators.required, Validators.minLength(13)]);
      dinamicForm.fantasy_name = new FormControl('', [Validators.required]);
      dinamicForm.responsible = new FormControl('', [Validators.required]);
      dinamicForm.genre = new FormControl('');
      dinamicForm.date_of_birth = new FormControl('');
      dinamicForm.cpf = new FormControl('');
      dinamicForm.rg = new FormControl('');
    } else if (type === 'pharmacist') {
      dinamicForm.pharmacy_id = new FormControl('', [Validators.required]);
      dinamicForm.rg = new FormControl('');
    }

    if (this.rule.test(this.userID)) {
      dinamicForm.user_id = new FormControl(this.userID);
    }

    this.dinamicForm = new FormGroup(dinamicForm);

    this.retrieveData(this.dataForm, this.dinamicForm);
  }

  /**
   * Insere os dados no formulário.
   */
  public retrieveData(data: any, form: FormGroup) {
    if (this.dataForm && this.rule.test(this.userID)) {
      const list = isArray(data) ? data : Object.keys(data);
      list.forEach(field => {
        if (form.controls[field] && (field.length > 0)) {
          form.controls[field].setValue(data[field]);
        }
        if (isArray(data[field])) {
          data[field].forEach(children => {
            this.retrieveData(children, form);
          });
        }
      });
    }
  }

  /**
   * Carrega os combos padrão.
   * @param allRouter - Lista de rotas de bsca
   */
  public getSelectList(allRouter: any) {
    Object.keys(allRouter).forEach(route => {
      if (allRouter[route]) {
        const list = route.replace('retrieve', '').toLowerCase();
        const listField = `${list}_title`;
        const field = listField.replace('ies_', 'y_');
        if (this.dataForm && this.dataForm[field]) {
          this.loadSelectListPaginate(list, allRouter[route], this.dataForm[field]);
        } else {
          this.loadSelectList(list, allRouter[route]);
        }
      }
    });
  }

  /**
   * Carrega os combos específicos ao carregar o componente.
   */
  public loadSpecificSelectListAuto() {
    Object.keys(this.autoLoadSelect).forEach(elements => {
      this[elements].optionSelectionChanges.subscribe(res => {
        if (res.isUserInput) {
          const list = this.autoLoadSelect[elements];
          this.loadSelectList(list, `combo-${list}`, res.source.value);
          if (this.autocompleFields[list]) {
            const route = `retrieve${list.charAt(0).toUpperCase() + list.slice(1)}`;
            this.routers[route] = `combo-${list}`;
          }
        }
      });
    });
  }

/**
 * Busca os dados dos combos.
 * @param list - Lista que deverá ser alimentada
 * @param route - Rota de busca dos objetos para a lista
 * @param parameter - Parâmetro opcional da busca
 */
  public loadSelectList(list: any, route: string, parameter = null) {
    this.restService.getAll(route, parameter, ['error'])
      .then((response: Select[])  => {
        this[list] = response;
        this.allowAutoComplete(list);
      });
  }

  /**
   * Busca os dados dos combos.
   * @param list - Lista que deverá ser alimentada
   * @param route - Rota de busca dos objetos para a lista
   * @param search - Parâmetro opcional da busca
   */
  public loadSelectListPaginate(list: any, route: string, search = null) {
    this.restService.getAllPaginate(route, false, 1, ['error'], search)
      .then((response: Select[])  => {
        this[list] = response;
        this.allowAutoComplete(list);
      });
  }

  /**
   * Resolve a formatação necessária para a API.
   */
  public formatFields(field: string) {
    const form =  { ...this.dinamicForm.value };
    form[field] = form[field].split('/').reverse().join('-');
    return form;
  }

  /**
   * Submete os dados do formulário.
   */
  public submitForm() {
    if (this.dinamicForm.valid) {
      this.formSubmitted = !this.formSubmitted;
      this.restService.postAll(
        this.finalRouteToPost,
        this.formatFields('date_of_birth'),
        this.serialized,
        ['success', 'error']
      ).then((response: any) => {
        this.formSubmitted = !this.formSubmitted;
        if (response.success) {
          EventEmitterService.get('submitedUserForm').emit({type: this.finalRouteToPost, response});
        }
      });
    }
  }

  /**
   * Permite a busca de campos com autocomplete.
   * @param list - Lista de carregamento de opções do autocomplete
   * @param field - Campo do autocomplete
   */
  private allowAutoComplete(list: any) {
    if (this.autocompleFields[list] &&
        this[list] && this[list].length > 0) {
      const field = this.autocompleFields[list];
      this[`${list}FilteredOptions`] = this.dinamicForm.controls[field].valueChanges
      .pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : value.display),
        map(value => value ? this._filter(value, list) : this[list].slice())
      );
    }
  }

  /**
   * Realiza a limpeza do campo que possui depência.
   * @param field - Campo do formulário
   */
  public clearNextField(field: string) {
    this.dinamicForm.controls[field].setValue('');
  }

  /**
   * Monitora a alteração de valor do autocomplete.
   * @param list - Lista da comparação
   */
  public displayFn(list: Select[]): (id: number) => string {
    return (value: number) => {
      const correspondingOption = Array.isArray(list) ? list.find(option => option.value === value) : null;
      return correspondingOption ? correspondingOption.display : '';
    };
  }

  /**
   * Realiza o filtro do valor do autocomplete.
   * @param display - campo de exibição
   * @param list - Lista de busca do campo
   */
  private _filter(display: string, list: any): Select[] {
      const filterValue = display.toLowerCase();
      const result = this[list].filter(option => option.display.toLowerCase().indexOf(filterValue) === 0);
      if (result.length > 0) {
        return result;
      } else if (this.autocompleFields[list]) {
        this.autoSearchOnDemand(filterValue, list).then((response) => {
          return response;
        });
      }
  }

  /**
   * Realiza o filtro do valor do autocomplete sob demanda
   * caso já não exista na lista.
   * @param search - Termo da busca
   * @param list - Lista de busca de campo
   */
  private autoSearchOnDemand(search: string, list: any) {
    const route = `retrieve${list.charAt(0).toUpperCase() + list.slice(1)}`;
    const newRoute = this.autocompleFields[list] ? `${this.routers[route]}/${this.dinamicForm.get('state_id').value}` : this.routers[route];
    return new Promise((resolve) => {
      this.restService.getAll(newRoute, search, false).then((response: any) => {
        this[list] = response;
        resolve(response);
      }).catch(error => {
        console.dir(error);
      });
    });
  }

  /**
   * Exibe os campos que estão com erro na validação.
   */
  public getFormValidationErrors() {
    const result = [];
    Object.keys(this.dinamicForm.controls).forEach(key => {
      const controlErrors: ValidationErrors = this.dinamicForm.get(key).errors;
      if (controlErrors) {
        Object.keys(controlErrors).forEach(keyError => {
          result.push({
            control: key,
            error: keyError,
            value: controlErrors[keyError]
          });
        });
      }
    });
    console.dir(result);
    return result;
  }

}
