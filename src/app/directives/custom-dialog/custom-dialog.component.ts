import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

export interface DialogInfo {
  title: string;
  content: Array<{
    title: string,
    description?: string,
    pdf?: string
  }>;
  cancelText: string;
  submitText: string;
}

@Component({
  selector: 'app-custom-dialog',
  templateUrl: './custom-dialog.component.html',
  styleUrls: ['./custom-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CustomDialogComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<CustomDialogComponent>,
              @Inject( MAT_DIALOG_DATA) public data: DialogInfo) {
    }

    ngOnInit(): void {
    }

    /**
     * Retorna a decisão ao componente de acinamento.
     * @param type - Dado de retorno do modal
     */
    public confirmDialog(type: boolean) {
      this.dialogRef.close(type);
    }

}
