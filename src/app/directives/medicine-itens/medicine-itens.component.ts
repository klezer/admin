import { Component, OnInit, Input, Output, ViewEncapsulation, EventEmitter } from '@angular/core';
import * as moment from 'moment';
import { FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { coerceNumberProperty } from '@angular/cdk/coercion';
import { RestService } from './../../services';

export interface Medicines {
  id: number;
  titulo: string;
  descricao: string;
  subtitulo: string;
  tarja: string;
}

/** Erro quando o controle inválido está sujo, é tocado ou enviado. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-medicine-itens, [app-medicine-itens]',
  templateUrl: './medicine-itens.component.html',
  styleUrls: ['./medicine-itens.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MedicineItensComponent implements OnInit {
  public delayTimer: any;
  public moment = moment;
  public matcher = new MyErrorStateMatcher();
  public stateCtrl = new FormControl();
  public filteredMedicines: Observable<Medicines[]>;
  public dosageList: any = [];
  public frequencyList: any = [];
  public formRecipe: FormGroup;
  public routers = {
      retrieveAllMedicines: 'https://crawler-drpediu.doc88.com.br/api/list-medicamentos',
      getAnamnesePatient: 'doctor-get-anamnese-patient'
  };
  public anamneseInfo: any = [];
  public medicineList: Medicines[] = [];
  public medicineSelectedValue: any;
  public itemMedicine: any = {
      dosage: null,
      considerations: null,
      frequency: null,
      number_of_days: null,
      title: null,
      subtitle: null,
      note: '',
      id: null,
      opened: true
  };
  public canAdd: boolean;
  public sliderDays = 0;
  public formDisable = false;
  public tickInterval = coerceNumberProperty('auto');
  public clickNumberControl = 0;
  public today = new Date();
  // tslint:disable-next-line:no-input-rename
  @Input('item') item?: any;
  // tslint:disable-next-line:no-input-rename
  @Input('index') index?: any;
  // tslint:disable-next-line:no-input-rename
  @Input('list') list?: any;
  @Output() form: EventEmitter<any> = new EventEmitter();
  @Output() valid: EventEmitter<any> = new EventEmitter();

  constructor(public restService: RestService) {
      this.formControl(false);
  }

  ngOnInit(): void {
    this.populateDosage();
    this.populateFrequency();
  }

  public filterTodayLimit(d: Date) {
    const today = new Date();
    return d > today;
  }

  /**
   * Definição do formulário.
   */
  public formControl(isRetrieve) {
    /** Formulário do Medicamentos */
    this.formRecipe = new FormGroup({
        medicine_id: new FormControl('', [Validators.required]),
        dosage: new FormControl({value: '', disabled: this.formDisable }, [Validators.required]),
        number_of_days: new FormControl('', [Validators.required]),
        frequency: new FormControl('', [Validators.required]),
        title: new FormControl('', [Validators.required]),
        subtitle: new FormControl('', [Validators.required]),
        note: new FormControl(''),
        considerations: new FormControl(''),
        validity_of_medicine: new FormControl(new Date(moment().add(31, 'days').format('YYYY-MM-DD')), [Validators.required])
    });
  }

  /**
   * Popula a quantidade de unidades.
   */
  public populateDosage() {
    let i = 1;
    for (; i <= 50; i++) {
        const unit = (i > 1) ? `${i} unidades` : `${i} unidade`;
        this.dosageList.push({ value: i, viewValue: unit});
    }
  }

  /**
   * Popula a quantidade de unidades.
   */
  public populateFrequency() {
    let i = 1;
    for (; i <= 24; i++) {
        const hour = (i > 1) ? `${i} horas` : `${i} hora`;
        this.frequencyList.push({ value: i, viewValue: hour});
    }
  }

  /**
   * Realiza a busca pela busca do usuário.
   */
  public filterMedicine(search: string): any {
    clearTimeout(this.delayTimer);
    this.delayTimer = setTimeout(() => {
      this.getMedicines(this.routers.retrieveAllMedicines, search).then((response: any) => {
        this.medicineList = response;
        return this.filteredMedicines = this.formRecipe.valueChanges.pipe(startWith(''), map(medicine => medicine ?
          this.filterMedicine(search) :
          this.medicineList.slice()));
      });
    }, 500);
  }

  /**
   * Seta os valores do medicamento escolhido.
   */
  public medicineSelected(event, medicine) {
    this.formRecipe.controls.medicine_id.setValue(medicine.id);
    this.formRecipe.controls.subtitle.setValue(medicine.subtitulo);
    this.formRecipe.controls.note.setValue(medicine.descricao);
  }

  /**
   * Busca todos os registros.
   */
  public getMedicines(router, obj): any {
    return new Promise((resolve) => {
      this.restService.getAllExternal(router, obj).then((response: any) => {
        resolve(response);
      });
    });
  }

  /**
   * Adiciona um novo medicamento a receita.
   */
    public confirmMedicine(item, position) {
      item.opened = false;
      // Ajusta o valor do período.
      this.formRecipe.controls.number_of_days.setValue(Math.round(this.formRecipe.get('number_of_days').value));
      // objeto de saída.
      const medicine = {
          index: position,
          new: true,
          value: this.formRecipe.value
      };
      this.form.emit(medicine);
      this.canAdd = true;
      if (this.clickNumberControl) {
        this.formRecipe.disable();
      }
      this.clickNumberControl = this.clickNumberControl + 1;
  }

  /**
   * Remove o medicamento da receita.
   */
  public removeMedicine(item) {
    if (this.list.length > 1) {
        const position = this.list.map((child) => child).indexOf(item);
        // objeto de saída.
        const medicine = {
            index: position,
            new: false,
            value: null
        };
        this.form.emit(medicine);
    }
  }

  /**
   * Adiciona um novo medicamento a receita.
   */
  public newMedicine() {
    this.formDisable = true;
    this.formRecipe.disable();
    this.list.push(this.itemMedicine);
    this.valid.emit(false);
  }

  /**
   * editMedicine
   */
  public editMedicine() {
    this.formDisable = false;
    this.canAdd = false;
    this.formRecipe.enable();
  }
}
