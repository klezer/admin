import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicineItensComponent } from './medicine-itens.component';

describe('MedicineItensComponent', () => {
  let component: MedicineItensComponent;
  let fixture: ComponentFixture<MedicineItensComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicineItensComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicineItensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
