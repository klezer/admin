import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RestService } from './../../services';

@Component({
  selector: 'app-list-items, [app-list-items]',
  templateUrl: './list-items.component.html',
  styleUrls: ['./list-items.component.scss']
})
export class ListItemsComponent implements OnInit {
  public list = [];
  @Input() private customRoute: string;
  @Input() public userID: any;
  @Output() private customRowClick?: EventEmitter<any> = new EventEmitter();
  constructor(private restService: RestService) { }

  ngOnInit() {
    this.getAll();
  }

  /**
   * Realiza a solicitação de todos os itens.
   */
  public getAll() {
    this.restService
    .getAll(
      this.customRoute,
      this.userID,
      ['error']
    )
    .then((response: any) => {
      this.list = response;
    });
  }

}
