import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomMaterialModule } from './../core/material.module';
// ProgressBar
import { MatProgressBarModule } from '@angular/material/progress-bar';
// Formulário
import { MatFormFieldModule } from '@angular/material/form-field';
// Formulário
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Paginator
import { MatPaginatorModule } from '@angular/material';
// Tooltip
import { MatTooltipModule } from '@angular/material/tooltip';
// Select
import { MatSelectModule } from '@angular/material/select';
// Autocomplete
import { MatAutocompleteModule } from '@angular/material/autocomplete';
// Toggle
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// Slider
import { MatSliderModule } from '@angular/material/slider';
// DatePicker
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
// Máscara de campos
import { NgxMaskModule } from 'ngx-mask';
// Diretivas
import { DataTableComponent } from './../directives/data-table/data-table.component';
import { CustomDialogComponent } from './../directives/custom-dialog/custom-dialog.component';
import { SafeUrl  } from './../pipes/safe-url/safe-url.service';
@NgModule({
  declarations: [
    DataTableComponent,
    CustomDialogComponent,
    SafeUrl
  ],
  imports: [
    CommonModule,
    CustomMaterialModule,
    MatProgressBarModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatTooltipModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatSlideToggleModule,
    MatSliderModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    NgxMaskModule.forRoot()
  ],
  providers: [MatDatepickerModule],
  entryComponents: [CustomDialogComponent],
  exports: [DataTableComponent]
})
export class CustomModulesModule { }
